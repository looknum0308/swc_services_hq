﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PM_WorkOrder50Hr.aspx.vb" Inherits="SWC_ServiceSystem.PM_WorkOrder50Hr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Service System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

      <script type = "text/javascript">
          function SetTarget() {
              document.forms[0].target = "_blank";
          }
</script>

</head>
<body>
     <form id="form1" runat="server">
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            <!-- Top Menu Items -->
   
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

             <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
           <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                    <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                     <%-- <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>   --%> 

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            PM Form <small></small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>
                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">
                       <%-- <div class="panel panel-default">--%>


         <asp:Panel ID="TB_PM50Hr" runat="server">
         <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <%--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--%>
                            <%--<i class="fa fa-info-circle"></i>  <strong>รายการตรวจเช็ค 50 ชั่วโมง หรือ 1 ปี</strong> --%>

                            <div class="breadcrumbs">
                                <a href="RepairOrder.aspx?ID=<%=Request.QueryString("ID") %>&UserID=<%=Request.QueryString("UserID") %>&Type=PM">Back</a> &nbsp;/&nbsp; <span>รายการตรวจเช็ค 50 ชั่วโมง หรือ 1 ปี</span>
                            </div>

                        </div>
                    </div>
                </div>
       <!-- /.row -->

                             <table  border="1" cellpadding="0" cellspacing="0" width="100%" id="Table1" class="table table-bordered table-hover" >
        <thead>
        <tr><td align="center" class="style17" colspan="2">
                    <asp:Label ID="Label26" runat="server" CssClass="fn_ContentTital" 
                        Text="รายการตรวจเช็ค" Font-Underline="True"></asp:Label>
                </td><td class="style17">
                    <asp:Label ID="Label24" runat="server" CssClass="fn_ContentTital" 
                        Text="ดำเนินการ"></asp:Label>
                </td>
            <td class="style13">
                    <asp:Label ID="Label25" runat="server" CssClass="fn_ContentTital" 
                        Text="หมายเหตุ"></asp:Label>
              
              
                </td>
        </tr>
        </thead>
        <tbody>
        <tr><td class="style22">
                    <asp:Label ID="Label23" runat="server" CssClass="fn_ContentTital" 
                        Text="1." ></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label22" runat="server" CssClass="style12" 
                        
                        Text="เปลี่ยนน้ำมันเครื่อง" 
                        Font-Bold="False"></asp:Label>
                        <br />
                </td>
            <td class="style24">
                <asp:CheckBox ID="chk1" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark1" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                </td>
        </tr>
       
        <tr><td class="style1">
                    <asp:Label ID="Label32" runat="server" CssClass="fn_ContentTital" 
                        Text="2."></asp:Label>
                </td><td  align="left" class="style1">
                    <asp:Label ID="Label31" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เปลี่ยนไส้กรองน้ำมันเครื่อง" 
                        Font-Bold="False"></asp:Label>
                </td>
            <td class="style1">
                <asp:CheckBox ID="chk2" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark2" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                </td>
            
        </tr>
       
        <tr><td class="style1">
                    <asp:Label ID="Label74" runat="server" CssClass="fn_ContentTital" 
                        Text="3."></asp:Label>
                </td><td  align="left" class="style1">
                    <asp:Label ID="Label1" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="ตรวจสอบระบบระบายไอเสียและ Flushing" 
                        Font-Bold="False" BorderColor="White" BorderWidth="3px"></asp:Label>
                </td>
            <td class="style1">
                <asp:CheckBox ID="chk3" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark3" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
            
        </tr>
       
     
        <tr><td class="style30">
                    <asp:Label ID="Label35" runat="server" CssClass="fn_ContentTital" 
                        Text="4."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label36" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="ตรวจสอบระบบเชื้อเพลิง" 
                        Font-Bold="False"></asp:Label>
                </td>
            <td class="style32">
                <%--                 <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatColumns="2" 
                    Width="100px">
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>0</asp:ListItem>
                </asp:RadioButtonList>--%>
                <asp:CheckBox ID="chk4" runat="server" Text="เรียบร้อย" />
                </td>
            <td class="style13">
                <asp:TextBox ID="Remark4" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                </td>
        </tr>
       

        <tr><td class="style21">
                    <asp:Label ID="Label39" runat="server" CssClass="fn_ContentTital" 
                        Text="5."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label40" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="ทดสอบระบบเชื้อเพลิงเพื่อเช็ครั่ว" 
                        Font-Bold="False"></asp:Label>
                    </td>
            <td>
                <asp:CheckBox ID="chk5" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark5" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
        

        <tr><td class="style21">
                    <asp:Label ID="Label49" runat="server" CssClass="fn_ContentTital" 
                        Text="6."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label41" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คลิ้นเร่ง" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk6" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark6" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>

        <tr><td class="style21">
                    <asp:Label ID="Label2" runat="server" CssClass="fn_ContentTital" 
                        Text="7."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label3" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็ค Fault code ด้วย B.U.D.S." 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk7" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark7" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr><td class="style21">
                    <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" 
                        Text="8."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คหัวเทียน" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk8" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark8" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr><td class="style21">
                    <asp:Label ID="Label6" runat="server" CssClass="fn_ContentTital" 
                        Text="9."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label7" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คความแน่นข้อต่อสายไฟทุกระบบ" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk9" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark9" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr><td class="style21">
                    <asp:Label ID="Label8" runat="server" CssClass="fn_ContentTital" 
                        Text="10."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label9" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="ทดสอบสวิทช์ดับเครื่องยนต์และเสียงเตือน" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk10" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark10" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr><td class="style21">
                    <asp:Label ID="Label10" runat="server" CssClass="fn_ContentTital" 
                        Text="11."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label11" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คสภาพข้อต่อและสายสลิงระบบบังคับเลี้ยว" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk11" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark11" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr><td class="style21">
                    <asp:Label ID="Label12" runat="server" CssClass="fn_ContentTital" 
                        Text="12."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label13" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คสภาพบูชของระบบบังคับเลี้ยว" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk12" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark12" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         <tr><td class="style21">
                    <asp:Label ID="Label14" runat="server" CssClass="fn_ContentTital" 
                        Text="13."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label15" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="ทดสอบการทำงานของ O.T.A.S." 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk13" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark13" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         <tr><td class="style21">
                    <asp:Label ID="Label16" runat="server" CssClass="fn_ContentTital" 
                        Text="14."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label17" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คซีลคาร์บอน, บูชยางเพลาขับ" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk14" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark14" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         <tr><td class="style21">
                    <asp:Label ID="Label19" runat="server" CssClass="fn_ContentTital" 
                        Text="15."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label20" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คสภาพบูช, ใบพัด และสภาพของ Wearing" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk15" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark15" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         <tr><td class="style21">
                    <asp:Label ID="Label21" runat="server" CssClass="fn_ContentTital" 
                        Text="16."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label27" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="ทดสอบระบบ iBR" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk16" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark16" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         <tr><td class="style21">
                    <asp:Label ID="Label28" runat="server" CssClass="fn_ContentTital" 
                        Text="17."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label29" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คสภาพฝาครอบ iBR" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk17" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark17" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         <tr><td class="style21">
                    <asp:Label ID="Label30" runat="server" CssClass="fn_ContentTital" 
                        Text="18."></asp:Label>
                </td><td  align="left">
                    <asp:Label ID="Label37" runat="server" CssClass="fn_ContentTital" 
                        
                        Text="เช็คแบตเตอรี่" 
                        Font-Bold="False"></asp:Label>
                    
                    <br />
                </td>
            <td>

                <asp:CheckBox ID="chk18" runat="server" Text="เรียบร้อย" />
            </td>
            <td class="style13">
                <asp:TextBox ID="Remark18" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            </td>
        </tr>
         </tbody>
        </table>              
                            </asp:Panel>           
                            
       <%-- End--%>


     <br />

                <div class="showback">
						&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" 
                            class="btn btn-info" />
                        &nbsp;<%--<button type="button" class="btn btn-warning">Warning</button>--%><asp:Button ID="btnClose" runat="server" Text="Cancel" 
                            class="btn btn-danger" />
                        &nbsp; <asp:Button ID="btnReport" runat="server" Text="Report" OnClientClick = "SetTarget();" class="btn btn-default" />
                            </div>
                        
                        <%--</div>--%>
                    </div>

                     

                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


     <%-- TAB--%>
          <script src="../vendor/jquery-1.7.1.min.js" type="text/javascript"></script> 
          <script src="../vendor/jquery.hashchange.min.js" type="text/javascript"></script>
          <script src="../lib/jquery.easytabs.min.js" type="text/javascript"></script>

          <style>
            /* Example Styles for Demo */
            .etabs { margin: 0; padding: 0; }
            .tab { display: inline-block; zoom:1; *display:inline; background: #eee; border: solid 1px #999; border-bottom: none; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0; }
            .tab a { font-size: 14px; line-height: 2em; display: block; padding: 0 10px; outline: none; }
            .tab a:hover { text-decoration: underline; }
            .tab.active { background: #fff; padding-top: 6px; position: relative; top: 1px; border-color: #666; }
            .tab a.active { font-weight: bold; }
            .tab-container .panel-container { background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
            .panel-container { margin-bottom: 10px; }
              .fn_ContentTital
              {}
              </style>

          <script type="text/javascript">
              $(document).ready(function () {
                  $('#tab-container').easytabs();
              });
          </script>

    <%-- TAB--%>

     

    <%--        dropdown--%><%--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>--%>
                    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                    
                   
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpCust").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpSale").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpSaleItem").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpReceptionnist").select2({
                             });
                         });
                    </script>
                    
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpStatus").select2({
                            });
                        });
                    </script>


</form>
</body>
</html>
