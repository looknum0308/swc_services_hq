﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ServiceMain.aspx.vb" Inherits="SWC_ServiceSystem.ServiceMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Service System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       
</head>
<body>
  <form id="form1" runat="server">
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                     <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                     <%-- <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>    --%>

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

               
              <div class="row">
                    <div class="col-lg-12">
                        <%--<div class="panel panel-default">--%>
                            
                            <%-- Start--%>
                          <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Siam Watercraft <small> Service System  </small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <%--<i class="fa fa-dashboard"></i> --%>
                                Repair Order
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

              
                <div class="row">
                <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                       <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><asp:Label ID="lblRO" runat="server" Text="124" Width="100%"></asp:Label></div>
                                        <div>Repair Order</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left"><a href="RepairOrderMain.aspx?type=RO&UserID=<%=Request.QueryString("UserID") %>">View Details</a></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                   <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><asp:Label ID="lblPM" runat="server" Text="124" Width="100%"></asp:Label></div>
                                        <div>Preventive Maintenance</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left"><a href="RepairOrderMain.aspx?Type=PM&UserID=<%=Request.QueryString("UserID") %>">View Details</a></span>
                                    
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><asp:Label ID="lblCL" runat="server" Text="12" Width="100%"></asp:Label></div>
                                        <div>Claim</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                     <span class="pull-left"><a href="RepairOrderMain.aspx?Type=CL&UserID=<%=Request.QueryString("UserID") %>">View Details</a></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                      <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><asp:Label ID="lblCA" runat="server" Text="12" Width="100%"></asp:Label></div>
                                        <div>Campaign</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                     <span class="pull-left"><a href="RepairOrderMain.aspx?Type=CA&UserID=<%=Request.QueryString("UserID") %>">View Details</a></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                    </div>

                <%--</div>--%>
               
               <div class="panel-body">
                                <div class="table-responsive">
                                  <h3>Repair Order </h3>
                        <asp:GridView ID="GridView1" CssClass="footable" runat="server" AutoGenerateColumns="false"
                    Style="max-width: 100%">
                    <Columns>
                       <asp:HyperLinkField DataNavigateUrlFields="ID,UserID,Type" 
                                            DataNavigateUrlFormatString="RepairOrder.aspx?ID={0}&UserID={1}&Type={2}" 
                                            DataTextField="ID" HeaderText="ID" 
                                            NavigateUrl="RepairOrder.aspx?ID={0}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>

                                            <ItemStyle Font-Bold="True" />
                                            <ControlStyle ForeColor = "Blue" />
                                        </asp:HyperLinkField>

                        <asp:BoundField DataField="CustName" HeaderText="CustomerName" />
                        <asp:BoundField DataField="ItemShow" HeaderText="Product" />
                        <asp:BoundField DataField="SerialNumberShow" HeaderText="SerialNumber" />
                        <asp:BoundField DataField="ClassisNumberShow" HeaderText="เลขตัวถัง" />
                        <%--<asp:BoundField DataField="Receptionnist_Name" HeaderText="Receptionnist_Name" />    --%>
                        <asp:BoundField DataField="RepairDate" HeaderText="Date" />
                        <%--<asp:BoundField DataField="CM_Status" HeaderText="Status" />--%>
                    </Columns>
                        </asp:GridView>
                                </div> 
                </div> 

               

                <%-- End--%>



                            
                        </div>
                    </div>

                     

                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


<%--Gridview--%>
<%--    https://www.aspsnippets.com/Articles/Bootstrap-Responsive-GridView-for-Mobile-Phone-Tablet-and-Desktop-display-in-ASPNet-using-jQuery.aspx--%>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
    rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('[id*=GridView1]').footable();
            });
            $(function () {
                $('[id*=GridView2]').footable();
            });
        </script>


</form>
</body>
</html>
