﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Web.CrystalReportViewerBase
Imports System.Data.OleDb
Imports System.IO
Imports CrystalDecisions.ReportSource
Imports System.Web.UI.WebControls.Table


Public Class RepairOrder
    Inherits System.Web.UI.Page
    Dim db_SWC As New Connect_Service
    Dim db_HR As New Connect_HR
    Dim sql1, sql, sqlRole As String
    Dim dt, dt1, dt2, dt3, dtt, dtRole As New DataTable
    Dim UserID, ID, Role, type As String
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        UserID = Request.QueryString("UserID")
        lblUserName.Text = UserID
        ID = Request.QueryString("ID")
        type = Request.QueryString("Type")


        If Not IsPostBack Then
            txtDateRepair.Text = Date.Now.ToString("dd/MM/yyyy")

            sql1 += " SELECT  [fullname]  FROM [TB_Employee] "
            sql1 += " Where  [employee_id] = '" & UserID & "'"
            dt1 = db_HR.GetDataTable(sql1)
            If dt1.Rows.Count > 0 Then
                lblUserName.Text = dt1.Rows(0)("fullname")
            End If

            sqlRole += " SELECT  [Role]  FROM [TB_Authorize] "
            sqlRole += " Where  [Username] = '" & UserID & "'"
            dtRole = db_SWC.GetDataTable(sqlRole)
            If dtRole.Rows.Count > 0 Then
                Role = dtRole.Rows(0)("Role")
            End If

            'Discount 100%
            If type = "CL" Or type = "CA" Then
                txtDiscount.Text = "100"
                drpDiscount.SelectedValue = "%"
                txtDiscount0.Text = "100"
                drpDiscount0.SelectedValue = "%"


            ElseIf type = "PM" Then
                lblPM.Visible = True
                drpPMType.Visible = True
                brnCheckList.Visible = True
            End If

            'Check New  หรือ old 
            sql += " SELECT * FROM [TB_RepiarOrder] "
            sql += " Where  ID = '" & ID & "'"
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count <= 0 Then
                'new
                btnSave.Visible = False
                GenID()
            Else
                txtID.Text = ID
                txtID.Enabled = False
                btnCreateIncident.Visible = False
                btnSave.Visible = True
                btnDelete.Visible = True
                btnReport.Visible = True
                btnDO.Visible = True
                'btnCancel.Visible = True
                loadCM(ID)

            End If

            'RefreshData()

        End If


        'If Me.IsPostBack Then
        '    TabName.Value = Request.Form(TabName.UniqueID)
        'End If

    End Sub


    'Private Sub RefreshData()
    '    Dim consString As String = ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString
    '    Using conn As New SqlConnection(consString)
    '        Dim cmd As New SqlCommand("select *  FROM [TB_Repair] where RepairID =1 union all SELECT * FROM [TB_Repair] where id = '" & ID & "'", conn)
    '        cmd.CommandType = CommandType.Text
    '        conn.Open()
    '        Dim drStudents As SqlDataReader = cmd.ExecuteReader()
    '        GridView1.DataSource = drStudents
    '        GridView1.DataBind()
    '    End Using
    'End Sub


    'Protected Sub AddNewStudent(sender As Object, e As EventArgs)
    '    System.Threading.Thread.Sleep(1000)
    '    ' To Test AJAX effect
    '    Dim Repair_Desc As String = DirectCast(GridView1.FooterRow.FindControl("txtRepair_Desc"), TextBox).Text
    '    Dim Repair_Cause As String = DirectCast(GridView1.FooterRow.FindControl("txtRepair_Cause"), TextBox).Text
    '    ' Dim IDservice As String = ID
    '    'Dim sAddress As String = DirectCast(GridView1.FooterRow.FindControl("txtAddress"), TextBox).Text

    '    If Repair_Desc = "" Then
    '        sms.Msg = "กรุณาระบุ รายการแจ้งซ่อม"
    '        Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
    '        Exit Sub
    '    End If

    '    Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString)
    '        Dim cmd As New SqlCommand()
    '        cmd.CommandType = CommandType.Text

    '        cmd.CommandText = "INSERT INTO TB_Repair(id, Repair_Desc, Repair_Cause,[CreateBy],[CreateDate]) " & "VALUES(@id, @Repair_Desc, @Repair_Cause, @CreateBy, @CreateDate );"

    '        'cmd.Parameters.Add("@Roll", SqlDbType.Int).Value = Convert.ToInt32(sRoll)
    '        cmd.Parameters.Add("@Repair_Desc", SqlDbType.VarChar).Value = Repair_Desc
    '        cmd.Parameters.Add("@Repair_Cause", SqlDbType.VarChar).Value = Repair_Cause
    '        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = ID
    '        cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now
    '        cmd.Parameters.Add("@CreateBy", SqlDbType.VarChar).Value = UserID

    '        con.Open()
    '        cmd.Connection = con
    '        cmd.ExecuteNonQuery()
    '    End Using
    '    RefreshData()

    'End Sub

    'Protected Sub EditStudent(sender As Object, e As GridViewEditEventArgs)
    '    GridView1.EditIndex = e.NewEditIndex
    '    RefreshData()
    'End Sub

    'Protected Sub UpdateStudent(sender As Object, e As GridViewUpdateEventArgs)
    '    '  Dim sRoll As String = DirectCast(GridView1.Rows(e.RowIndex).FindControl("lblID"), TextBox).Text
    '    Dim Repair_Desc As String = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtRepair_Desc"), TextBox).Text
    '    Dim Repair_Cause As String = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtRepair_Cause"), TextBox).Text
    '    Dim RepairID As Integer = Convert.ToInt32(GridView1.DataKeys(e.RowIndex).Value)

    '    Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString)
    '        Dim cmd As New SqlCommand()
    '        cmd.CommandType = CommandType.Text
    '        ' cmd.CommandText = "UPDATE Student set RepairID=@RepairID," & "Name=@Name,Email=@Email,Address=@Address,AdDate=@AdDate where Roll=@PrevRoll;"
    '        cmd.CommandText = "UPDATE TB_Repair set Repair_Desc=@Repair_Desc ," & " Repair_Cause=@Repair_Cause ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate where RepairID=@RepairID;"

    '        ' cmd.Parameters.Add("@Roll", SqlDbType.Int).Value = Convert.ToInt32(sRoll)
    '        cmd.Parameters.Add("@Repair_Desc", SqlDbType.VarChar).Value = Repair_Desc
    '        cmd.Parameters.Add("@Repair_Cause", SqlDbType.VarChar).Value = Repair_Cause
    '        cmd.Parameters.Add("@ModifyDate", SqlDbType.DateTime).Value = DateTime.Now
    '        cmd.Parameters.Add("@ModifyBy", SqlDbType.VarChar).Value = UserID
    '        cmd.Parameters.Add("@RepairID", SqlDbType.Int).Value = RepairID

    '        con.Open()
    '        cmd.Connection = con
    '        cmd.ExecuteNonQuery()
    '        GridView1.EditIndex = -1
    '    End Using
    '    RefreshData()
    'End Sub

    'Protected Sub CancelStudent(sender As Object, e As GridViewCancelEditEventArgs)
    '    GridView1.EditIndex = -1
    '    RefreshData()
    'End Sub

    'Protected Sub DeleteStudent(sender As Object, e As EventArgs)
    '    Dim nRoll As Integer = Convert.ToInt32(DirectCast(sender, ImageButton).CommandArgument)

    '    Using con As New SqlConnection(ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString)
    '        Dim cmd As New SqlCommand()
    '        cmd.CommandType = CommandType.Text
    '        cmd.CommandText = "DELETE FROM TB_Repair WHERE RepairID=@RepairID;"

    '        cmd.Parameters.Add("@RepairID", SqlDbType.Int).Value = nRoll

    '        con.Open()
    '        cmd.Connection = con
    '        cmd.ExecuteNonQuery()
    '    End Using
    '    RefreshData()
    'End Sub


    Sub GenID()

        If txtID.Enabled = True Then
            ' New ID
            Dim conn As New Connect_Service
            Dim sql As String = "Select * from [TB_RepiarOrder] where len(ID) = 12 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring([ID],3,2) and type = '" & type & "'"
            Dim sql2 As String = "Select Max(Right(ID,6)) as ID from [TB_RepiarOrder] where len(ID) = 12 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring([ID],3,2) and type = '" & type & "'"

            dt = db_SWC.GetDataTable(sql)
            dt2 = db_SWC.GetDataTable(sql2)

            If dt.Rows.Count <= 0 Then
                If type = "RO" Then
                    txtID.Text = "RO" & Now.ToString("yy") & Now.ToString("MM") & "000001"
                ElseIf type = "PM" Then
                    txtID.Text = "PM" & Now.ToString("yy") & Now.ToString("MM") & "000001"
                ElseIf type = "CL" Then
                    txtID.Text = "CL" & Now.ToString("yy") & Now.ToString("MM") & "000001"
                ElseIf type = "CA" Then
                    txtID.Text = "CA" & Now.ToString("yy") & Now.ToString("MM") & "000001"
                End If

            Else
                Dim newID As Integer
                If type = "RO" Then
                    newID = CInt(dt2.Rows(0)("ID"))
                    newID += 1
                    txtID.Text = "RO" & Now.ToString("yy") & Now.ToString("MM") & newID.ToString("000000")
                ElseIf type = "PM" Then
                    newID = CInt(dt2.Rows(0)("ID"))
                    newID += 1
                    txtID.Text = "PM" & Now.ToString("yy") & Now.ToString("MM") & newID.ToString("000000")
                ElseIf type = "CL" Then
                    newID = CInt(dt2.Rows(0)("ID"))
                    newID += 1
                    txtID.Text = "CL" & Now.ToString("yy") & Now.ToString("MM") & newID.ToString("000000")
                ElseIf type = "CA" Then
                    newID = CInt(dt2.Rows(0)("ID"))
                    newID += 1
                    txtID.Text = "CA" & Now.ToString("yy") & Now.ToString("MM") & newID.ToString("000000")
                End If
            End If
        Else
        End If
    End Sub

    'Protected Sub RefreshGridView()
    '    Dim dt As DataTable = New DataTable()
    '    Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("CheckinConnectionString").ConnectionString)
    '        Dim ad As SqlDataAdapter = New SqlDataAdapter("SELECT [ItemId],[ItemDesc],[Qty] from [VW_PartsWH] ", conn) ' where SaleId = '" & drpSale.SelectedValue & "'", conn)
    '        ad.Fill(dt)
    '        ViewState("dtSupplier") = dt
    '    End Using

    '    GVSparePart.DataSource = dt
    '    GVSparePart.DataBind()
    'End Sub

    Protected Sub drpCust_Init(sender As Object, e As EventArgs) Handles drpCust.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull([CustomerId],'') as id,[CustName] from [VW_CustomerSale] where CustomerId is not null group by   CustomerId , CustName order by CustomerId "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpCust.Items.Clear()
        drpCust.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpCust.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpCust_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpCust.SelectedIndexChanged

        sql1 += " SELECT  [CustAdd]  ,[CustPhone],CustName  FROM [VW_CustomerSale] "
        sql1 += " Where  [CustomerId] = '" & drpCust.SelectedValue & "'"
        dt1 = db_SWC.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblCustAdd.Text = dt1.Rows(0)("CustAdd")
            lblCustTel.Text = dt1.Rows(0)("CustPhone")
            lblCustName.Text = dt1.Rows(0)("CustName")
        End If

        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull([ItemId],'') as id,[ItemDesc]  from [VW_CustomerSale] where  ItemId is not null and [CustomerId] like '" & drpCust.SelectedValue & "' group by ItemId,ItemDesc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpItem.Items.Clear()
        drpItem.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While

    End Sub


    'Protected Sub cmdDlete_Click(sender As Object, e As EventArgs) Handles cmdDlete.Click

    '    ' PerformDelete()
    '    Dim str As String = String.Empty
    '    Dim strname As String = String.Empty
    '    For Each gvrow As GridViewRow In GVSparePart.Rows
    '        Dim chk As CheckBox = DirectCast(gvrow.FindControl("chk"), CheckBox)
    '        If chk IsNot Nothing And chk.Checked Then
    '            str += GVSparePart.DataKeys(gvrow.RowIndex).Value.ToString() + ","c
    '            strname += gvrow.Cells(2).Text & ","c
    '        End If
    '    Next

    '    str = str.Trim(",".ToCharArray())
    '    strname = strname.Trim(",".ToCharArray())
    '    lblmsg.Text = "Selected UserIds: <b>" & str & "</b><br/>" & "Selected UserNames: <b>" & strname & "</b>"

    'End Sub

    Public Function PerformDelete(GV As GridView, sTableName As String) As Boolean
        Dim bSaved As Boolean = False
        Dim sClause As String = "''"
        Dim sSQL As String = ""
        Dim comm As SqlCommand

        Dim sConstr As String = ConfigurationManager.ConnectionStrings("ServiceConnectionString").ConnectionString

        For Each oItem As GridViewRow In GV.Rows
            If CType(oItem.FindControl("chk"), CheckBox).Checked Then
                sClause += "," + GV.DataKeys(oItem.DataItemIndex).Value.ToString()
            End If
        Next

        sSQL = "DELETE FROM " + sTableName + " WHERE " + GV.DataKeyNames.GetValue(0) + " IN(" + sClause + ")"
        Using conn As SqlConnection = New SqlConnection(sConstr)
            conn.Open()
            comm = New SqlCommand(sSQL, conn)
            Using comm
                comm.CommandTimeout = 0
                comm.ExecuteNonQuery()
                bSaved = True
            End Using
        End Using
        Return bSaved
    End Function

    Protected Sub drpReceptionnist_Init(sender As Object, e As EventArgs) Handles drpReceptionnist.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        ' sql = "select [employee_id] as id,[fullname] from [TB_Employee]   where department_id = 'B0109'  "

        sql = "select [employee_id] as id,[fullname] from [TB_Employee]  where department_id in ('B0109','BSWC004','BSWC009')  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpReceptionnist.Items.Clear()
        drpReceptionnist.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpReceptionnist.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


    Sub loadCM(ByVal ID As String)
        'sql = " SELECT ID ,ReferJob	 ,Customer_id   ,CustName	      ,CustAdd	      ,CustPhone	      ,item	      ,Serialnumber ,	 ClassisNumber     ,Receptionnist "
        'sql += " ,RepairDate ,Technician  ,Type,TypePM  ,Inspector	     ,Consignee	      ,stat	 ,[Series] ,[color] ,[HoursOfUse] ,[Miles]     ,CreateBy	      ,CreateDate	      ,ModifyBy	      ,ModifyDate"
        sql = " SELECT * "
        sql += " from [VW_RepairOrder] "
        sql += " where ID = '" & ID & "'"

        dt = db_SWC.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtID.Text = dt.Rows(0)("ID")
            drpJob.SelectedValue = dt.Rows(0)("ReferJob")
            txtDateRepair.Text = dt.Rows(0)("RepairDate")
            drpCust.SelectedValue = dt.Rows(0)("Customer_id")
            drpItem.SelectedValue = dt.Rows(0)("item")
            drpSerialNumber.SelectedValue = dt.Rows(0)("Serialnumber")
            drpClassisNumber.SelectedValue = dt.Rows(0)("ClassisNumber")
            txtItem.Text = dt.Rows(0)("ItemOther")
            txtSerialNumber.Text = dt.Rows(0)("SerialNumberOther")
            txtClassisNumber.Text = dt.Rows(0)("ClassisNumberOther")

            drpStatus.SelectedValue = dt.Rows(0)("stat")
            drpReceptionnist.SelectedValue = dt.Rows(0)("Receptionnist")
            drpTechnician.SelectedValue = dt.Rows(0)("Technician")
            drpCust.SelectedValue = dt.Rows(0)("Customer_id")
            lblCustName.Text = dt.Rows(0)("CustName")
            lblCustAdd.Text = dt.Rows(0)("CustAdd")
            lblCustTel.Text = dt.Rows(0)("CustPhone")

            txtSeries.Text = dt.Rows(0)("Series")
            txtColor.Text = dt.Rows(0)("color")
            txtHour.Text = dt.Rows(0)("HoursOfUse")
            txtMiles.Text = dt.Rows(0)("Miles")
            drpPMType.SelectedValue = dt.Rows(0)("TypePM")

            If dt.Rows(0)("jobType") = "Internal Job" Then
                rdoType.Items.FindByValue("Internal Job").Selected = True
            ElseIf dt.Rows(0)("jobType") = "Regular Job" Then
                rdoType.Items.FindByValue("Regular Job").Selected = True
            End If

        End If

    End Sub

    'Protected Sub btnADD_Click(sender As Object, e As EventArgs) Handles btnADD.Click

    '    'insert()
    '    'insert to CM and Repair

    '    If txtID.Enabled = True Then
    '        GenID()

    '        If drpCust.SelectedValue = "1" Then
    '            sms.Msg = "กรุณาระบุ ข้อมูลลูกค้า"
    '            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
    '            Exit Sub
    '        End If

    '        If txtRepairDesc.Text = "" Then
    '            sms.Msg = "กรุณาระบุรายการแจ้งซ่อม"
    '            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
    '            Exit Sub
    '        End If

    '        ''Main
    '        Dim sql As String = "INSERT INTO [TB_RepiarOrder] (ID,Customer_id ,[Item],[SerialNumber],[ClassisNumber] "
    '        sql &= ",Receptionnist ,RepairDate,Stat, CreateBy, CreateDate, Technician,[Series],[Color],[HoursOfUse],[Miles],[type],TypePM ,ReferJob"
    '        ' sql &= ",Technician ,Inspector ,Consignee"
    '        sql &= ")"
    '        sql &= "VALUES ('" & txtID.Text & "','" & drpCust.SelectedValue & "','" & drpItem.SelectedValue & "','" & drpSerialNumber.SelectedValue & "','" & drpClassisNumber.SelectedValue & "'"
    '        sql &= ",'" & drpReceptionnist.SelectedValue & "','" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "' "
    '        sql &= ",'" & drpStatus.SelectedValue & "'"
    '        sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "','" & drpTechnician.SelectedValue & "' "
    '        sql &= ",'" & txtSeries.Text & "','" & txtColor.Text & "','" & txtHour.Text & "','" & txtMiles.Text & "','" & type & "','" & drpPMType.SelectedValue & "','" & drpJob.SelectedValue & "' )"
    '        dtt = db_SWC.GetDataTable(sql)

    '        'Repair
    '        Dim sql0 As String = "INSERT INTO [TB_Repair] ([ID]  ,[Repair_Desc] ,[Repair_Cause], CreateBy, CreateDate)"
    '        sql0 &= "VALUES ('" & txtID.Text & "','" & txtRepairDesc.Text & "','" & txtRepairCause.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
    '        dtt = db_SWC.GetDataTable(sql0)
    '        GridView1.DataBind()


    '        txtID.Enabled = False
    '        btnCreateIncident.Visible = False
    '        btnSave.Visible = True
    '        btnDelete.Visible = True
    '        btnReport.Visible = True
    '        btnDO.Visible = True
    '        ' btnCancel.Visible = True

    '    Else
    '        If txtRepairDesc.Text = "" Then
    '            sms.Msg = "กรุณาระบุรายการแจ้งซ่อม"
    '            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
    '            Exit Sub
    '        End If


    '        Dim sql As String = "INSERT INTO [TB_Repair] ([ID]  ,[Repair_Desc] ,[Repair_Cause], CreateBy, CreateDate)"
    '        sql &= "VALUES ('" & txtID.Text & "','" & txtRepairDesc.Text & "','" & txtRepairCause.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
    '        dtt = db_SWC.GetDataTable(sql)

    '        GridView1.DataBind()

    '    End If

    '    txtRepairDesc.Text = ""
    '    txtRepairCause.Text = ""

    'End Sub

    Protected Sub btnCreateIncident_Click(sender As Object, e As EventArgs) Handles btnCreateIncident.Click
        Dim JobType As String

        If txtID.Enabled = True Then

            If drpCust.SelectedValue = "0" Then
                sms.Msg = "กรุณาระบุ ข้อมูลลูกค้า"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            If type = "PM" And drpPMType.SelectedValue = "0" Then
                sms.Msg = "กรุณาระบุประเภทการตรวจเช็ค"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            If rdoType.Items.FindByValue("Internal Job").Selected = False And rdoType.Items.FindByValue("Regular Job").Selected = False Then
                sms.Msg = "กรุณาระบุประเภทงาน"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            If rdoType.Items.FindByValue("Internal Job").Selected = True Then
                JobType = "Internal Job"
            ElseIf rdoType.Items.FindByValue("Regular Job").Selected = True Then
                JobType = "Regular Job"
            Else
                JobType = ""
            End If

            GenID()

            Dim sql As String = "INSERT INTO [TB_RepiarOrder] (ID,Customer_id ,[Item],[SerialNumber],[ClassisNumber] "
            sql &= ",Receptionnist ,RepairDate,Stat, CreateBy, CreateDate, Technician,[Series],[Color],[HoursOfUse],[Miles],[type],TypePM ,ReferJob ,ItemOther,SerialNumberOther, ClassisNumberOther"
            sql &= ",DateOpen,JobType"
            sql &= ")"
            sql &= "VALUES ('" & txtID.Text & "','" & drpCust.SelectedValue & "','" & drpItem.SelectedValue & "','" & drpSerialNumber.SelectedValue & "','" & drpClassisNumber.SelectedValue & "'"
            sql &= ",'" & drpReceptionnist.SelectedValue & "','" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "' "
            sql &= ",'" & drpStatus.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "','" & drpTechnician.SelectedValue & "' "
            sql &= ",'" & txtSeries.Text & "','" & txtColor.Text & "','" & txtHour.Text & "','" & txtMiles.Text & "','" & type & "','" & drpPMType.SelectedValue & "','" & drpJob.SelectedValue & "'"
            sql &= ",'" & txtItem.Text & "','" & txtSerialNumber.Text & "','" & txtClassisNumber.Text & "' ,getdate() , '" & JobType & "')"
            dtt = db_SWC.GetDataTable(sql)

            txtID.Enabled = False
            btnCreateIncident.Visible = False
            btnSave.Visible = True
            btnDelete.Visible = True
            btnReport.Visible = True
            btnDO.Visible = True
        Else
            'Update
            Update()
        End If


    End Sub

    Sub Update()
        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim sqlcon As New OleDbConnection(db_SWC.cnPAStr)
        Dim sqlcom As New OleDbCommand
        Dim JobType As String

        Dim sSql As String = "UPDATE [TB_RepiarOrder] SET  "
        sSql += " [Customer_id]='" & drpCust.SelectedValue & "',"
        sSql += " [Item]='" & drpItem.SelectedValue & "',"
        sSql += " [SerialNumber]='" & drpSerialNumber.SelectedValue & "',"
        sSql += " [ClassisNumber]='" & drpClassisNumber.SelectedValue & "',"

        sSql += " [ItemOther]='" & txtItem.Text & "',"
        sSql += " [SerialNumberOther]='" & txtSerialNumber.Text & "',"
        sSql += " [ClassisNumberOther]='" & txtClassisNumber.Text & "',"

        sSql += " [Receptionnist]='" & drpReceptionnist.SelectedValue & "',"
        sSql += " [Technician]='" & drpTechnician.SelectedValue & "',"
        sSql += " [RepairDate]='" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "',"
        sSql += " [Stat]='" & drpStatus.SelectedValue & "',"

        If drpStatus.SelectedValue = "Waiting for spares" Then
            sSql += " [DateWaitingforSpares] = getdate() ,"
        ElseIf drpStatus.SelectedValue = "Close" Then
            sSql += " [DateClose] = getdate() ,"
        ElseIf drpStatus.SelectedValue = "Cancel" Then
            sSql += " [DateCancel] = getdate() ,"
        End If

        If rdoType.Items.FindByValue("Internal Job").Selected = True Then
            sSql += " [JobType] = 'Internal Job',"
        ElseIf rdoType.Items.FindByValue("Regular Job").Selected = True Then
            sSql += " [JobType] = 'Regular Job',"
        End If

        ',[Series],[Color],[HoursOfUse],[Miles]
        sSql += " [Series]='" & txtSeries.Text & "',"
        sSql += " [Color]='" & txtColor.Text & "',"
        sSql += " [HoursOfUse]='" & txtHour.Text & "',"
        sSql += " [Miles]='" & txtMiles.Text & "',"

        sSql += " ModifyBy = '" & UserID & "',"
        sSql += " ModifyDate = '" & Date.Now.ToString("yyyy-MM-dd") & "'"


        sSql += " Where ID = '" & txtID.Text & "'"

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(db_SWC.cnPAStr)
            sqlcon.Open()
        End If
        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With
    End Sub


    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If rdoType.Items.FindByValue("Internal Job").Selected = False And rdoType.Items.FindByValue("Regular Job").Selected = False Then
            sms.Msg = "กรุณาระบุประเภทงาน"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        Update()
    End Sub


    Protected Sub btnAddLabor_Click(sender As Object, e As EventArgs) Handles btnAddLabor.Click
        If txtID.Enabled = True Then
            GenID()

            'Main
            Dim sql As String = "INSERT INTO [TB_RepiarOrder] (ID,Customer_id ,[Item],[SerialNumber],[ClassisNumber] "
            sql &= ",Receptionnist ,RepairDate,Stat, CreateBy, CreateDate, Technician,[Series],[Color],[HoursOfUse],[Miles],[type],TypePM ,ReferJob"
            ' sql &= ",Technician ,Inspector ,Consignee"
            sql &= ")"
            sql &= "VALUES ('" & txtID.Text & "','" & drpCust.SelectedValue & "','" & drpItem.SelectedValue & "','" & drpSerialNumber.SelectedValue & "','" & drpClassisNumber.SelectedValue & "'"
            sql &= ",'" & drpReceptionnist.SelectedValue & "','" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "' "
            sql &= ",'" & drpStatus.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "','" & drpTechnician.SelectedValue & "' "
            sql &= ",'" & txtSeries.Text & "','" & txtColor.Text & "','" & txtHour.Text & "','" & txtMiles.Text & "','" & type & "','" & drpPMType.SelectedValue & "','" & drpJob.SelectedValue & "' )"
            dtt = db_SWC.GetDataTable(sql)

            txtID.Enabled = False
            btnCreateIncident.Visible = False
            btnSave.Visible = True
            btnDelete.Visible = True
            btnReport.Visible = True
            btnDO.Visible = True
            'btnCancel.Visible = True

            'Labor
            If txtHours.Text = "" Then
                sms.Msg = "กรุณาระบุจำนวนชั่วโมงทำงาน !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            If drpDiscount.SelectedValue = "Percent" And txtDiscount.Text > 100 Then
                sms.Msg = "กรุณาระบุ % ใหม่ !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If


            'Dim sql0 As String = "INSERT INTO [TB_Labor] ([ID] , LaborCode,DescriptionL,LaborRate ,WorkingHours,Discount,DiscountType, CreateBy, CreateDate)"
            'sql0 &= "VALUES ('" & txtID.Text & "','Labor','" & txtLaborCode.Text & "','" & txtLaborRate.Text & "','" & txtHours.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            'dtt = db_SWC.GetDataTable(sql0)

            Dim sql0 As String = "INSERT INTO [TB_Labor] ([ID] , LaborCode,DescriptionL,LaborRate ,WorkingHours,Discount,DiscountType, CreateBy, CreateDate)"
            sql0 &= "VALUES ('" & txtID.Text & "','Labor','" & txtLaborCode.Text & "','" & txtLaborRate.Text & "','" & txtHours.Text & "' "
            sql0 &= ",'" & txtDiscount.Text & "','" & drpDiscount.SelectedValue & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql0)
            'GridView2.DataBind()

        Else
            If txtHours.Text = "" Then
                sms.Msg = "กรุณาระบุจำนวนชั่วโมงทำงาน !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            If drpDiscount.SelectedValue = "Percent" And txtDiscount.Text > 100 Then
                sms.Msg = "กรุณาระบุ % ใหม่ !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            Dim sql As String = "INSERT INTO [TB_Labor] ([ID] , LaborCode,DescriptionL,LaborRate ,WorkingHours,Discount,DiscountType, CreateBy, CreateDate)"
            sql &= "VALUES ('" & txtID.Text & "','Labor','" & txtLaborCode.Text & "','" & txtLaborRate.Text & "','" & txtHours.Text & "' "
            sql &= ",'" & txtDiscount.Text & "','" & drpDiscount.SelectedValue & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            'GridView2.DataBind()

        End If

        txtLaborCode.Text = ""

        txtLaborRate.Text = ""
        txtHours.Text = ""
        GridView2.DataBind()

    End Sub

    'Part Required

    Protected Sub drpPartNo_Init(sender As Object, e As EventArgs) Handles drpPartNo.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from [VW_PartsWH] ", conn) ' where SaleId = '" & drpSale.SelectedValue & "'", conn

        sql = "select [ItemId] as id,[ItemDesc]  from [VW_PartsWH] where  ItemId is not null  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpPartNo.Items.Clear()
        drpPartNo.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpPartNo.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpPartNo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpPartNo.SelectedIndexChanged
        sql1 += " SELECT  [itemdesc]  ,[qty],UnitPrice  FROM [VW_PartsWH] "
        sql1 += " Where  [ItemId] = '" & drpPartNo.SelectedValue & "'"
        dt1 = db_SWC.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblDescriptionPart.Text = dt1.Rows(0)("itemdesc")
            txtQty.Text = dt1.Rows(0)("qty")
            txtUnitPrice.Text = dt1.Rows(0)("UnitPrice")
        End If
    End Sub

    Protected Sub txtQtyUse_TextChanged(sender As Object, e As EventArgs) Handles txtQtyUse.TextChanged
        If txtQtyUse.Text > txtQty.Text Then
            sms.Msg = "QTY not enough !!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

    End Sub

    Protected Sub btnAddPart_Click(sender As Object, e As EventArgs) Handles btnAddPart.Click
        If txtID.Enabled = True Then
            GenID()


            'Main

            Dim sql As String = "INSERT INTO [TB_RepiarOrder] (ID,Customer_id ,[Item],[SerialNumber],[ClassisNumber] "
            sql &= ",Receptionnist ,RepairDate,Stat, CreateBy, CreateDate, Technician,[Series],[Color],[HoursOfUse],[Miles],[type],TypePM ,ReferJob"
            ' sql &= ",Technician ,Inspector ,Consignee"
            sql &= ")"
            sql &= "VALUES ('" & txtID.Text & "','" & drpCust.SelectedValue & "','" & drpItem.SelectedValue & "','" & drpSerialNumber.SelectedValue & "','" & drpClassisNumber.SelectedValue & "'"
            sql &= ",'" & drpReceptionnist.SelectedValue & "','" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "' "
            sql &= ",'" & drpStatus.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "','" & drpTechnician.SelectedValue & "' "
            sql &= ",'" & txtSeries.Text & "','" & txtColor.Text & "','" & txtHour.Text & "','" & txtMiles.Text & "','" & type & "','" & drpPMType.SelectedValue & "','" & drpJob.SelectedValue & "' )"
            dtt = db_SWC.GetDataTable(sql)

            txtID.Enabled = False
            btnCreateIncident.Visible = False
            btnSave.Visible = True
            btnDelete.Visible = True
            btnReport.Visible = True
            btnDO.Visible = True
            '  btnCancel.Visible = True

            'Part
            If txtQtyUse.Text = "" Then
                sms.Msg = "กรุณาระบุจำนวน !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If
            If drpDiscount0.SelectedValue = "Percent" And txtDiscount0.Text > 100 Then
                sms.Msg = "กรุณาระบุ % ใหม่ !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            'Dim sql0 As String = "INSERT INTO [TB_PartRequired] ([ID] ,PartNo,Description,QtyUse ,Price,Discount,DiscountType, CreateBy, CreateDate)"
            'sql0 &= "VALUES ('" & txtID.Text & "','" & drpPartNo.SelectedValue & "','" & lblDescriptionPart.Text & "','" & txtQtyUse.Text & "','" & txtUnitPrice.Text & "'"
            'sql0 &= ",'" & txtDiscount0.Text & "','" & drpDiscount0.SelectedValue & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            'dtt = db_SWC.GetDataTable(sql0)

            Dim query As String = "INSERT INTO TB_PartRequired ("
            query &= "[ID] ,PartNo,Description,QtyUse ,Price,Discount,DiscountType, CreateBy, CreateDate"
            query &= ")"
            query += " VALUES( "
            query &= "@ID ,@PartNo,@Description,@QtyUse ,@Price,@Discount,@DiscountType, @CreateBy, @CreateDate"
            query &= ")"

            Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString

            Using con As SqlConnection = New SqlConnection(constr)
                Using cmd As SqlCommand = New SqlCommand(query)
                    cmd.Parameters.AddWithValue("@ID", txtID.Text)
                    cmd.Parameters.AddWithValue("@PartNo", drpPartNo.SelectedValue)
                    cmd.Parameters.AddWithValue("@Description", lblDescriptionPart.Text)
                    cmd.Parameters.AddWithValue("@QtyUse", txtQtyUse.Text)
                    cmd.Parameters.AddWithValue("@Price", txtUnitPrice.Text)
                    cmd.Parameters.AddWithValue("@Discount", txtDiscount0.Text)
                    cmd.Parameters.AddWithValue("@DiscountType", drpDiscount0.SelectedValue)
                    cmd.Parameters.AddWithValue("@CreateBy", UserID)        
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                    cmd.Connection = con
                    con.Open()
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Using
            End Using


        Else
            If txtQtyUse.Text = "" Then
                sms.Msg = "กรุณาระบุจำนวน !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If
            If drpDiscount0.SelectedValue = "Percent" And txtDiscount0.Text > 100 Then
                sms.Msg = "กรุณาระบุ % ใหม่ !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            'Dim sql As String = "INSERT INTO [TB_PartRequired] ([ID] ,PartNo,Description,QtyUse ,Price, Discount,DiscountType, CreateBy, CreateDate)"
            'sql &= "VALUES ('" & txtID.Text & "','" & drpPartNo.SelectedValue & "','" & lblDescriptionPart.Text & "','" & txtQtyUse.Text & "','" & txtUnitPrice.Text & "'"
            'sql &= ",'" & txtDiscount0.Text & "','" & drpDiscount0.SelectedValue & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            'dtt = db_SWC.GetDataTable(sql)
            Dim query As String = "INSERT INTO TB_PartRequired ("
            query &= "[ID] ,PartNo,Description,QtyUse ,Price,Discount,DiscountType, CreateBy, CreateDate"
            query &= ")"
            query += " VALUES( "
            query &= "@ID ,@PartNo,@Description,@QtyUse ,@Price,@Discount,@DiscountType, @CreateBy, @CreateDate"
            query &= ")"

            Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString

            Using con As SqlConnection = New SqlConnection(constr)
                Using cmd As SqlCommand = New SqlCommand(query)
                    cmd.Parameters.AddWithValue("@ID", txtID.Text)
                    cmd.Parameters.AddWithValue("@PartNo", drpPartNo.SelectedValue)
                    cmd.Parameters.AddWithValue("@Description", lblDescriptionPart.Text)
                    cmd.Parameters.AddWithValue("@QtyUse", txtQtyUse.Text)
                    cmd.Parameters.AddWithValue("@Price", txtUnitPrice.Text)
                    cmd.Parameters.AddWithValue("@Discount", txtDiscount0.Text)
                    cmd.Parameters.AddWithValue("@DiscountType", drpDiscount0.SelectedValue)
                    cmd.Parameters.AddWithValue("@CreateBy", UserID)
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                    cmd.Connection = con
                    con.Open()
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Using
            End Using
            'GridView3.DataBind()

        End If

        drpPartNo.SelectedValue = "1"
        lblDescriptionPart.Text = ""
        txtQty.Text = ""
        txtUnitPrice.Text = ""
        txtQtyUse.Text = ""
        GridView3.DataBind()

    End Sub

    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click


        If type = "PM" Then
            If drpPMType.SelectedValue = "0" Then
                sms.Msg = "กรุณาระบุประเภทการตรวจเช็ค"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            ElseIf drpPMType.SelectedValue = "50Hr" Then
                'Dim sqlReport As String = " 1=1 "
                'If ID <> "" Then
                '    sqlReport = sqlReport & " and {VW_PM50.ID} = '" & ID & "' "
                'End If
                'Server.Transfer("RepairOrderReport.aspx?Form=PM50&sql=" & sqlReport)
                Server.Transfer("RepairOrderReport.aspx?Type=PM50&id=" & txtID.Text)
            ElseIf drpPMType.SelectedValue = "100Hr" Then
                'Dim sqlReport As String = " 1=1 "
                'If ID <> "" Then
                '    sqlReport = sqlReport & " and {VW_PM100.ID} = '" & ID & "' "
                'End If
                'Server.Transfer("RepairOrderReport.aspx?Form=PM100&sql=" & sqlReport)
                Server.Transfer("RepairOrderReport.aspx?Type=PM100&id=" & txtID.Text)
            End If

        Else
            Dim sqlReport As String = " 1=1 "

            '{VW_CM.ID}="CM17000005"
            'If txtID.Text <> "" Then
            '    sqlReport = sqlReport & " and {VW_RepairOrder.ID} = '" & txtID.Text & "' "
            'End If
            'Server.Transfer("RepairOrderReport.aspx?Form=RepariOrder&sql=" & sqlReport)

            Server.Transfer("RepairOrderReport.aspx?Type=RepariOrder&id=" & txtID.Text)


        End If



    End Sub


    Protected Sub drpTechnician_Init(sender As Object, e As EventArgs) Handles drpTechnician.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee]  where department_id in ('B0109','BSWC004')  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpTechnician.Items.Clear()
        drpTechnician.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpTechnician.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click

        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim sqlcon As New OleDbConnection(db_SWC.cnPAStr)
        Dim sqlcom As New OleDbCommand

        Dim sSql As String = "UPDATE [TB_RepiarOrder] SET  "
        'sSql += " [Customer_id]='" & drpCust.SelectedValue & "',"
        'sSql += " [Sale_ID]='" & drpSale.SelectedValue & "',"
        'sSql += " [Sale_ItemID]='" & drpSaleItem.SelectedValue & "',"
        'sSql += " [Receptionnist]='" & drpReceptionnist.SelectedValue & "',"
        'sSql += " [Technician]='" & drpTechnician.SelectedValue & "',"
        'sSql += " [RepairDate]='" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "',"
        sSql += " [Stat]='Delete',"
        sSql += " ModifyBy = '" & UserID & "',"
        sSql += " ModifyDate = '" & Date.Now.ToString("yyyy-MM-dd") & "'"

        sSql += " Where ID = '" & txtID.Text & "'"

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(db_SWC.cnPAStr)
            sqlcon.Open()
        End If
        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With

        Server.Transfer("RepairOrderMain.aspx?UserID=" & UserID)

    End Sub

    Protected Sub drpItem_Init(sender As Object, e As EventArgs) Handles drpItem.Init

        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        If drpCust.SelectedValue <> "0" Then
            sql = "select 'อิ่นๆ','โปรดระบุข้อมูลช่องถัดไป..' union all select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where [CustomerId] like '" & drpCust.SelectedValue & "' and itemid is not null group by [ItemId] ,[ItemDesc]  "

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If
            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpItem.Items.Clear()
            drpItem.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            End While
        Else
            sql = "select 'อิ่นๆ','โปรดระบุข้อมูลช่องถัดไป..' union all select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where itemid is not null group by [ItemId] ,[ItemDesc]   "

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If
            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpItem.Items.Clear()
            drpItem.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            End While
        End If


    End Sub

    Protected Sub drpItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpItem.SelectedIndexChanged
        '----------
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from VW_CustomerSale where SaleId = '" & drpSale.SelectedValue & "'", conn)
        sql = "select 'อิ่นๆ' union all select isnull([SerialNumber],'') as id  from [VW_CustomerSale] where  [ItemID] like '" & drpItem.SelectedValue & "'  group by SerialNumber"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSerialNumber.Items.Clear()
        drpSerialNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub drpSerialNumber_Init(sender As Object, e As EventArgs) Handles drpSerialNumber.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        If drpItem.SelectedValue <> "1" Then
            sql = "select 'อิ่นๆ' union all select isnull([SerialNumber],'') as id  from [VW_CustomerSale] where SerialNumber is not null  and [ItemID] like '" & drpItem.SelectedValue & "' group by SerialNumber"

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If

            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpSerialNumber.Items.Clear()
            drpSerialNumber.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0)))
            End While
        Else
            sql = "select 'อิ่นๆ' union all select isnull([SerialNumber],'') as id  from [VW_CustomerSale] where SerialNumber is not null   group by SerialNumber"

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If

            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpSerialNumber.Items.Clear()
            drpSerialNumber.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0)))
            End While
        End If

    End Sub

    Protected Sub drpSerialNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSerialNumber.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select 'อิ่นๆ' union all  select isnull([ChassisNumber],'') as id  from [VW_CustomerSale] where ChassisNumber is not null and [SerialNumber] like '" & drpSerialNumber.SelectedValue & "'  group by ChassisNumber"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpClassisNumber.Items.Clear()
        drpClassisNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpClassisNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub



    Protected Sub drpClassisNumber_Init(sender As Object, e As EventArgs) Handles drpClassisNumber.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        If drpSerialNumber.SelectedValue <> "1" Then
            sql = "select 'อิ่นๆ' union all  select isnull([ChassisNumber],'') as id  from [VW_CustomerSale] where ChassisNumber is not null and  [SerialNumber] like '" & drpSerialNumber.SelectedValue & "' "

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If
            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpClassisNumber.Items.Clear()
            drpClassisNumber.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpClassisNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
            End While
        Else
            sql = "select 'อิ่นๆ' union all  select  isnull([ChassisNumber],'') as id  from [VW_CustomerSale] where ChassisNumber is not null  "

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If
            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpClassisNumber.Items.Clear()
            drpClassisNumber.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpClassisNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
            End While
        End If

    End Sub


    Protected Sub btnDO_Click(sender As Object, e As EventArgs) Handles btnDO.Click
        'Dim sqlReport As String = " 1=1 "

        ''{VW_CM.ID}="CM17000005"
        'If txtID.Text <> "" Then
        '    sqlReport = sqlReport & " and {VW_DeliveryOrder.ID} = '" & txtID.Text & "' "
        'End If

        'Server.Transfer("RepairOrderReport.aspx?Form=DO&sql=" & sqlReport)
        Server.Transfer("RepairOrderReport.aspx?Form=DO&id=" & txtID.Text)

    End Sub

    Protected Sub brnCheckList_Click(sender As Object, e As EventArgs) Handles brnCheckList.Click
        If drpPMType.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุประเภทการตรวจเช็ค"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        ElseIf drpPMType.SelectedValue = "50Hr" Then
            Server.Transfer("PM_WorkOrder50Hr.aspx?&ID=" & txtID.Text & "&UserID=" & UserID)
        ElseIf drpPMType.SelectedValue = "100Hr" Then
            Server.Transfer("PM_WorkOrder100Hr.aspx?&ID=" & txtID.Text & "&UserID=" & UserID)
        End If
    End Sub


    Protected Sub drpJob_Init(sender As Object, e As EventArgs) Handles drpJob.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [ID] as id,[Item] from [TB_RepiarOrder]   "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpJob.Items.Clear()
        drpJob.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpJob.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub Insert(sender As Object, e As EventArgs)
        SqlDataSource1.Insert()
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow AndAlso GridView1.EditIndex <> e.Row.RowIndex Then
            TryCast(e.Row.Cells(2).Controls(2), LinkButton).Attributes("onclick") = "return confirm('Do you want to delete this row?');"
        End If
    End Sub


End Class