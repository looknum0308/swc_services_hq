﻿Imports Microsoft.VisualBasic
Public Class PKMsg
    Inherits System.Web.UI.Page
    Private sMsg As String
    Property Msg() As String
        Get
            Return sMsg
        End Get
        Set(ByVal value As String)
            sMsg = value
        End Set
    End Property

    Public Sub New(ByVal Msg As String)
        sMsg = Msg
    End Sub

    Public Function ShowMsgBox() As String
        Dim message As String
        message = "<script language='javascript'>"
        message += "alert('" & sMsg & "');"
        message += "</script>"
        Return message
    End Function

    Public Function ConfirmMsgBox() As String
        Dim message As String
        message = "<script language='javascript'>"
        message += "var a=confirm('" & sMsg & "');"
        message += "if (a) {window.open('ShowMainType.aspx');}"
        message += "</script>"
        Return message
    End Function

    Public Function WritetempCtrlval(ByVal nID As String, ByVal nVal As String, ByVal nIP As String, Optional ByVal nSessionID As String = "") As Integer
        Dim x As Integer = 1
        Session("wRequest" & nID & nSessionID & nIP) = nVal
        Return x
    End Function

    Public Function ReadtempCtrlval(ByVal nID As String, ByVal nIP As String, Optional ByVal nSessionID As String = "") As String
        Dim s As String = Session("wRequest" & nID & nSessionID & nIP)
        Session("wRequest" & nID & nSessionID & nIP) = Nothing
        If s Is Nothing Then s = ""
        Return s
    End Function

    Public Function CleartempVal() As Integer
        Session.Clear()
    End Function

End Class
