﻿Imports System.Net

Public Class API
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MsgBox(ReturnOmeglePOST("http://omegle.com/start?rcs=1"))
    End Sub



    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(ReturnOmegleGET("http://omegle.com/events"))
    End Sub

   

    Public Function ReturnOmeglePOST(ByVal URL As String) As String
        Dim uri As New Uri(URL)
        If (uri.Scheme = uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Post
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()
            response.Close()
            Return tmp
        End If
        Return Nothing
    End Function


    Public Function ReturnOmegleGET(ByVal URL As String) As String
        Dim uri As New Uri(URL)
        If (uri.Scheme = uri.UriSchemeHttp) Then
            Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            request.Method = WebRequestMethods.Http.Get
            Dim response As HttpWebResponse = request.GetResponse()
            Dim reader As New IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()
            response.Close()
            Return tmp
        End If
        Return "FAIL"
    End Function




End Class