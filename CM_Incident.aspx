﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CM_Incident.aspx.vb" Inherits="SWC_ServiceSystem.CM_Incident" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Service System</title>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

           
    <style type="text/css">
        .style4
        {
            height: 39px;
        }
        .style5
        {
            height: 42px;
        }
    </style>

           
</head>
<body>
     <form id="form1" runat="server">
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Service System</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                   
                      <li>
                                <a href="#"><i class="fa fa-fw fa-arrows-v"></i>Corrective Maintenance</a>
                        </li>
                            
                    <li> 
                        <a href="CM_Incident.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Incident</span>
                      </a>

                    </li>
                    <li>
                           <a  href="CM_CreateIncident.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Create Incident</span>
                      </a>
                    </li>
                    <li>
                       <a  href="CM_Report.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Report</span>
                      </a>
                    </li>
                        <%--  <h5>  Preventive Maintenance</h5>    --%>
                  
                        <li>
                                <a href="#"><i class="fa fa-fw fa-arrows-v"></i>Preventive Maintenance</a>
                        </li>


                   <%-- <li>
                        <a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                    <li>
                        <a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>--%><%--<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>--%>
                    <li>
                        <a href="blank-page.html"><i class="fa fa-fw fa-desktop"></i>Job</a>
                    </li>
                    <li>
                        <a href="index-rtl.html"><i class="fa fa-fw fa-edit""></i>Create Job</a>
                    </li>
                     <li>
                        <a href="index-rtl.html"><i class="fa fa-fw fa-table"></i>Report</a>
                    </li>
                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Repair Order <small>ใบสั่งซ่อม</small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>

                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">

                <table >
            <tr>
                <td align="left" class="style4">

                    </td>
                <td align="left" class="style4">

                    <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" 
                        Text="Customer : " ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style4">


                <asp:DropDownList ID="drpCust" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>


                    </td>
                <td align="left" class="style4">


                    <asp:Label ID="lblCustName" runat="server" CssClass="fn_ContentTital" 
                        Text="Customer name" ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style5">

                    </td>
                <td align="left" class="style5">

      
                    <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" 
                        Text="Repair Order Date : " ToolTip="ชื่อลูกค้า"></asp:Label>

      
                    </td>
                <td align="left" class="style5">

                  <asp:TextBox ID="txtDateRepair" runat="server" CssClass="fn_Content" Width="150px"  
                         class="form-control" Font-Size="Small" ReadOnly="True"></asp:TextBox>


                </td>
                <td align="left" class="style5">
                    </td>
            </tr>       
            
          
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13" colspan="3">

                <asp:Button ID="btnSearch" runat="server" Text="Search" 
                            class="btn btn-info" />
                    &nbsp;
                 <asp:Button ID="btnExport" runat="server" Text="Export" class="btn btn-default" />	   
                    
                    </td>
            </tr>       
            
          
        </table>

        <br />
        
    
          <asp:GridView ID="GridView" 
                Font-Size="Medium" 
                AutoGenerateColumns="False" 
                onrowdatabound="GridView_RowDataBound"
                runat="server" Width="100%" PageSize="15">
                
       
                <Columns>
                      <asp:HyperLinkField DataNavigateUrlFields="CMID,UserID" 
                                            DataNavigateUrlFormatString="CM_CreateIncident.aspx?CMID={0}&UserID={1}" 
                                            DataTextField="CMID" HeaderText="เลขที่ใบสั่งซ่อม" 
                                            NavigateUrl="CM_CreateIncident.aspx?CMID={0}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>

                                            <ItemStyle Font-Bold="True" />
                                            <ControlStyle ForeColor = "Blue" />
                                        </asp:HyperLinkField>


                       <%--<asp:HyperLinkField DataNavigateUrlFields="LeaveID,EmpID" 
                                            DataNavigateUrlFormatString="LeaveOnline_HR.aspx?LeaveID={0}&EmpID={1}" 

                                            DataTextField="LeaveID" HeaderText="เลขที่ใบลา" 
                                            NavigateUrl="LeaveOnline_HR.aspx?LeaveID={0}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>

                                            <ItemStyle Font-Bold="True" />
                                            <ControlStyle ForeColor = "Blue" />
                                        </asp:HyperLinkField>--%>



                    
                    <asp:TemplateField HeaderText ="รหัสลูกค้า">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomer_id" runat ="server" Text ='<%# Eval("Customer_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                       <asp:TemplateField HeaderText ="ชื่อลูกค้า">
                        <ItemTemplate>
                            <asp:Label ID="lblCustName" runat ="server" Text ='<%# Eval("CustName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText ="วันที่แจ้งซ่อม">
                        <ItemTemplate>
                            <asp:Label ID="lblRepairDate" runat ="server" Text ='<%# Eval("RepairDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText ="พนักงานรับแจ้ง">
                        <ItemTemplate>
                            <asp:Label ID="lblReceptionnist_Name" runat ="server" Text ='<%# Eval("Receptionnist_Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText ="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblCM_Status" runat ="server" Text ='<%# Eval("CM_Status") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
            </asp:GridView>



                        </div>
                    </div>
                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


   
     <script>
         // SCRIPT FOR THE MOUSE EVENT.
         function MouseEvents(objRef, evt) {
             if (evt.type == "mouseover") {
                 objRef.style.cursor = 'pointer';
                 objRef.style.backgroundColor = "#EEEED1";
             }
             else {
                 if (evt.type == "mouseout") objRef.style.backgroundColor = "#FFF";
             }
         }
        </script>


     <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtDateRepair.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });     
      </script>


</form>
</body>

</html>
