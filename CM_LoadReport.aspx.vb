﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Web.CrystalReportViewerBase
Imports System.Data.OleDb
Imports System.IO
Imports CrystalDecisions.ReportSource
Imports System.Web.UI.WebControls.Table

Public Class CM_LoadReport
    Inherits System.Web.UI.Page
    Dim db As New Connect_HR
    Dim ids As String
    Dim uid As String
    Dim uapp As String
    Dim usr() As String
    Dim sql As String
    Dim dt As New DataTable
    Dim sess As String
    Dim sqlPara1 As SqlParameter
    Dim sqlPara2 As SqlParameter
    Dim fname As String
    Dim Formula As FormulaFieldDefinition
    Dim sqlReport, Form As String
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As Table
        Dim cryRpt As New ReportDocument

        Form = Request.QueryString(0)
        sqlReport = Request.QueryString(1)


        If Form = "CM" Then
            cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_RepairOrder.rpt"))

        ElseIf Form = "CMExcel" Then
            cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_RepairOrderExcel.rpt"))

        ElseIf Form = "PM50" Then
            cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_WorkOrderPM50.rpt"))

        ElseIf Form = "PM100" Then
            cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_WorkOrderPM100.rpt"))

        ElseIf Form = "PMExcel" Then
            cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_WorkOrderExcel.rpt"))

        End If


        Call CreateCondition()
        If sql <> "" Then
            cryRpt.RecordSelectionFormula = sql
        End If


        With crConnectionInfo
            .ServerName = "10.1.1.20"
            .DatabaseName = "DB_Service"
            .UserID = "sa"
            .Password = "Quax_005"
        End With


        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next

        If Form = "CM" Then
            CrystalReportViewer1.ID = "CM_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "CMExcel" Then
            CrystalReportViewer1.ID = "CMExcel_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "PM50" Then
            CrystalReportViewer1.ID = "PM50_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "PM100" Then
            CrystalReportViewer1.ID = "PM100_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "PMExcel" Then
            CrystalReportViewer1.ID = "PMExcel_" & Date.Now.ToString("dd/MM/yyyy")

        End If

        CrystalReportViewer1.ReportSource = cryRpt
        CrystalReportViewer1.RefreshReport()

    End Sub

    Sub CreateCondition()
        If sqlReport <> "" Then
            sql = sqlReport
        End If

    End Sub

End Class