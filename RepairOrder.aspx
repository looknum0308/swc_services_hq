﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RepairOrder.aspx.vb" Inherits="SWC_ServiceSystem.RepairOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Service System</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <%--TAB--%><%--     http://www.tutorialrepublic.com/faq/how-to-keep-the-current-tab-active-on-page-reload-in-bootstrap.php--%><%--<link rel="stylesheet" href="css/bootstrap.min.css">--%>
    <%--<link rel="stylesheet" href="css/bootstrap-theme.min.css">--%>
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }

            $('#btadd_deposit').click(function () {

                $('#modaledit').modal('toggle');
            })

        });
    </script>
    <script type="text/javascript">
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                     <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                     <%-- <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>    --%>

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>


                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Repair Order <small>ใบสั่งซ่อม</small>
                        </h2>
                        <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <%--Start TAB--%>
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a data-toggle="tab" href="#sectionA">REPAIR ORDER FORM</a></li>
                            <li><a data-toggle="tab" href="#sectionB">REPAIR ORDER Detail</a></li>
                            <li><a data-toggle="tab" href="#sectionC">PARTS REQUIRED</a></li>
                            <li><a data-toggle="tab" href="#sectionD">LABOR</a></li>
                            <li><a data-toggle="tab" href="#sectionE">เงินมัดจำ</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="sectionA" class="tab-pane fade in active">
                                <br />
                                <table width="100%" dir="ltr">
                                    <tr>
                                        <td align="right" class="style17">
                                            <asp:Label ID="Label9" runat="server" Text="เลขที่ NO. :" Width="87px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style17" colspan="2">
                                            <asp:TextBox ID="txtID" runat="server" CssClass="fn_Content" Width="150px" class="form-control"
                                                Font-Size="Small" ReadOnly="True" Height="30px"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style17">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style17">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style17">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style17">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style17">
                                            <asp:Label ID="Label18" runat="server" Text="ประเภทงาน :" Width="87px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style17" colspan="2">
                                            <asp:RadioButtonList ID="rdoType" runat="server" RepeatColumns="2" Width="100%">
                                                <asp:ListItem>Internal Job</asp:ListItem>
                                                <asp:ListItem>Regular Job</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td align="right" class="style17">
                                            <asp:Label ID="Label48" runat="server" CssClass="fn_ContentTital" Text="Job อ้างอิง :"
                                                Font-Size="Small" Width="70px"></asp:Label>
                                        </td>
                                        <td align="left" class="style17">
                                            <asp:DropDownList ID="drpJob" runat="server" Width="100%" class="form-control" ClientIDMode="Static"
                                                Font-Size="Small" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" class="style17">
                                        </td>
                                        <td align="left" class="style17">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style14">
                                            <asp:Label ID="Label63" runat="server" Text="วันที่แจ้งซ่อม :" Width="87px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style14" colspan="2">
                                            <asp:TextBox ID="txtDateRepair" runat="server" CssClass="fn_Content" Width="150px"
                                                class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>
                                        </td>
                                        <td align="right" class="style14">
                                            <asp:Label ID="Label88" runat="server" CssClass="fn_ContentTital" Text="สถานะ :"
                                                Font-Size="Small" Width="45px"></asp:Label>
                                        </td>
                                        <td align="left" class="style14">
                                            <asp:DropDownList ID="drpStatus" runat="server" Width="100%" class="form-control"
                                                ClientIDMode="Static" Font-Size="Small">
                                                <asp:ListItem>Open</asp:ListItem>
                                                <asp:ListItem>Waiting for spares</asp:ListItem>
                                                <asp:ListItem>Close</asp:ListItem>
                                                <asp:ListItem>Cancel</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" class="style14">
                                        </td>
                                        <td align="left" class="style14">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style4">
                                            <asp:Label ID="Label64" runat="server" CssClass="fn_ContentTital" Text="พนักงานรับแจ้ง :"
                                                Font-Size="Small" Width="90px"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style5" colspan="2">
                                            <asp:DropDownList ID="drpReceptionnist" runat="server" Width="100%" class="form-control"
                                                ClientIDMode="Static" Font-Size="Small">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="style5">
                                            <asp:Label ID="Label70" runat="server" CssClass="fn_ContentTital" Text="ช่างผู้รับผิดชอบ :"
                                                Font-Size="Small" Width="90px"></asp:Label>
                                        </td>
                                        <td align="left" class="style5">
                                            <asp:DropDownList ID="drpTechnician" runat="server" Width="100%" class="form-control"
                                                ClientIDMode="Static" Font-Size="Small">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" class="style6">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style5">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style4">
                                            &nbsp;
                                        </td>
                                        <td align="left" valign="middle" class="style5" colspan="2">
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style5">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style5">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style6">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style5">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style4">
                                            <asp:Label ID="Label53" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="Red"
                                                Text="*"></asp:Label>
                                            <asp:Label ID="Label33" runat="server" CssClass="fn_ContentTital" Text="ลูกค้า :"
                                                Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style5" colspan="2">
                                            <asp:DropDownList ID="drpCust" runat="server" Width="100%" class="form-control" ClientIDMode="Static"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                            <%-- <div class="form-group">
                    <label for="gender1" class="col-sm-2 control-label">With Bootstrap:</label>
                    <div class="col-sm-2">
                    <select class="form-control" id="gender1">
                      <option>Male</option>
                      <option>Female</option>
                    </select>          
          
                        </div>
                </div>          --%>
                                        </td>
                                        <td align="right" class="style5">
                                            <asp:Label ID="Label62" runat="server" CssClass="fn_ContentTital" Text="ชื่อลูกค้า :"
                                                Height="21px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style5">
                                            <asp:Label ID="lblCustName" runat="server" Text="-" Width="100%"></asp:Label>
                                        </td>
                                        <td align="left" class="style6">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style5">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style4">
                                            <asp:Label ID="Label60" runat="server" CssClass="fn_ContentTital" Text="ที่อยู่ :"
                                                Width="99px" Height="21px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style5" colspan="2">
                                            <asp:Label ID="lblCustAdd" runat="server" Text="-" Width="100%"></asp:Label>
                                        </td>
                                        <td align="right" class="style5">
                                            <asp:Label ID="Label34" runat="server" CssClass="fn_ContentTital" Text="เบอร์โทรศัพท์ :"
                                                Width="90px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style5">
                                            <asp:Label ID="lblCustTel" runat="server" Text="-" Width="100%"></asp:Label>
                                        </td>
                                        <td align="left" class="style6">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style5">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style3">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style2">
                                            &nbsp;
                                        </td>
                                        <td align="right">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style7" colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style14">
                                            <asp:Label ID="Label59" runat="server" CssClass="fn_ContentTital" Text="Product :"
                                                Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style14" colspan="2">
                                            <asp:DropDownList ID="drpItem" runat="server" AutoPostBack="True" ClientIDMode="Static"
                                                class="form-control" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="style14">
                                            <asp:Label ID="Label89" runat="server" Text="อื่นๆ ระบุ :" Width="90px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style14">
                                            <asp:TextBox ID="txtItem" runat="server" CssClass="fn_Content" Width="100%" class="form-control"
                                                Font-Size="Small" Height="30px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style15">
                                            &nbsp;<asp:Label ID="Label65" runat="server" CssClass="fn_ContentTital" Text="Serial Number :"
                                                Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style15" colspan="2">
                                            <asp:DropDownList ID="drpSerialNumber" runat="server" ClientIDMode="Static" class="form-control"
                                                Width="100%" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="style14">
                                            <asp:Label ID="Label7" runat="server" CssClass="fn_ContentTital" Text="อื่นๆ ระบุ :"
                                                Width="90px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style15">
                                            <asp:TextBox ID="txtSerialNumber" runat="server" CssClass="fn_Content" Width="100%"
                                                class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>
                                        </td>
                                        <td align="left" class="style15">
                                        </td>
                                        <td align="left" class="style15">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style16">
                                            &nbsp;<asp:Label ID="Label80" runat="server" CssClass="fn_ContentTital" Text="เลขตัวถัง :"
                                                Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style16" colspan="2">
                                            <asp:DropDownList ID="drpClassisNumber" runat="server" ClientIDMode="Static" class="form-control"
                                                Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="style14">
                                            <asp:Label ID="Label8" runat="server" CssClass="fn_ContentTital" Text="อื่นๆ ระบุ :"
                                                Width="90px" Font-Size="Small"></asp:Label>
                                        </td>
                                        <td align="left" class="style16">
                                            <asp:TextBox ID="txtClassisNumber" runat="server" CssClass="fn_Content" Width="100%"
                                                class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>
                                        </td>
                                        <td align="left" class="style16">
                                        </td>
                                        <td align="left" class="style16">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style16">
                                            <asp:Label ID="Label85" runat="server" CssClass="fn_ContentTital" Text="รุ่น :" Font-Size="Small"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16" colspan="2">
                                            <asp:TextBox ID="txtSeries" runat="server" CssClass="fn_Content" Width="200px" class="form-control"
                                                Font-Size="Small" Height="30px"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style16">
                                            <asp:Label ID="Label83" runat="server" CssClass="fn_ContentTital" Text="Miles :"
                                                Width="90px" Font-Size="Small"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16">
                                            <asp:TextBox ID="txtMiles" runat="server" CssClass="fn_Content" Width="200px" class="form-control"
                                                Font-Size="Small" Height="30px"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style16">
                                            <asp:Label ID="Label2" runat="server" CssClass="fn_ContentTital" Text="สี :" Font-Size="Small"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16" colspan="2">
                                            <asp:TextBox ID="txtColor" runat="server" CssClass="fn_Content" Width="200px" class="form-control"
                                                Font-Size="Small" Height="30px"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style16">
                                            <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" Text="ชั่วโมงการใช้งาน :"
                                                Font-Size="Small"></asp:Label>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16">
                                            <asp:TextBox ID="txtHour" runat="server" CssClass="fn_Content" Width="200px" class="form-control"
                                                Font-Size="Small" Height="30px"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style16">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style8">
                                        </td>
                                        <td align="left" valign="middle" colspan="6" class="style8">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style3">
                                            <asp:Label ID="lblPM" runat="server" CssClass="fn_ContentTital" Text="ประเภทการตรวจเช็ค PM :"
                                                Font-Size="Small" Visible="False"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" colspan="3">
                                            <asp:DropDownList ID="drpPMType" runat="server" AutoPostBack="True" class="form-control"
                                                ClientIDMode="Static" Height="30px" Width="100%" Visible="False">
                                                <asp:ListItem Value="0">--เลือก--</asp:ListItem>
                                                <asp:ListItem Value="50Hr">ตรวจเช็ค 50 ชั่วโมงหรือ 1 ปี</asp:ListItem>
                                                <asp:ListItem Value="100Hr">ตรวจเช็ค 100 ชั่วโมงหรือ 1 ปี</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="middle" colspan="3">
                                            &nbsp;<asp:Button ID="brnCheckList" runat="server" Text="Check List" class="btn btn-primary"
                                                Visible="False" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="sectionB" class="tab-pane fade">
                                <table>
                                    <tr>
                                        <td align="left" class="style13">
                                            <h4>
                                                แจ้งซ่อม</h4>
                                        </td>
                                        <td align="left" class="style1">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <%-- <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                            DataKeyNames="RepairID" 
                            RowStyle-BackColor="" 
                            AlternatingRowStyle-BackColor = ""
                             HeaderStyle-BackColor="#6495ED" 
                            HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" CellPadding="3" 
                    ShowFooter = "True" onrowediting="EditStudent" onrowupdating="UpdateStudent"  
                            onrowcancelingedit="CancelStudent" BackColor="White" BorderColor="#999999" 
                            BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" 
                            GridLines="Vertical" Width="100%">
                        <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                    <asp:TemplateField HeaderText = "ID">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("RepairID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
 
                    <asp:TemplateField HeaderText = "Repair_Desc">
                        <ItemTemplate>
                            <asp:Label ID="lblRepair_Desc" runat="server" Text='<%# Eval("Repair_Desc")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRepair_Desc" runat="server"  Text='<%# Eval("Repair_Desc")%>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRepair_Desc" runat="server"></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
 
                    <asp:TemplateField HeaderText = "Repair_Cause">
                        <ItemTemplate>
                            <asp:Label ID="lblRepair_Cause" runat="server" Text='<%# Eval("Repair_Cause")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtRepair_Cause" runat="server"  Text='<%# Eval("Repair_Cause")%>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtRepair_Cause" runat="server"></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>
              
               <asp:CommandField  ShowEditButton="True" ButtonType="Image" EditText="Edit Record" 
                            CancelText="Cancel Update" UpdateText="Update Record" 
                            EditImageUrl="~/Images/icon_edit.ico" ControlStyle-Height="30px" 
                            ControlStyle-Width="30px" CancelImageUrl="~/Images/icon_close.ico" 
                            UpdateImageUrl="~/Images/icon_Save.ico">
                        <ControlStyle Height="30px" Width="30px" />
                        </asp:CommandField>


                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/icon_delete.ico" ToolTip="Delete Record" Width="30px" Height="30px" CommandArgument = '<%# Eval("RepairID")%>' OnClientClick = "return confirm('Are you sure to Delete?')" OnClick = "DeleteStudent"/>
                        </ItemTemplate>
 
                        <FooterTemplate>
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/add.ico" ToolTip="Add Record" OnClick = "AddNewStudent" Width="30px" Height="30px"/>
                        </FooterTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                    </Columns>
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#808080" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#383838" />
                    </asp:GridView>
                    </ContentTemplate>
                    </asp:UpdatePanel>--%>
                                <%--                    <asp:GridView ID="GridView4" runat="server" AllowPaging="True"  
                                                            
            AutoGenerateColumns="False" DataKeyNames="PartID" DataSourceID="SqlDataSource3" 
                                                            Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Small" ShowFooter="True" 
                                                            Width="100%" 
            CellPadding="3" BackColor="White" BorderColor="#999999" 
                                                           
                                                            GridLines="Vertical" BorderWidth="1px" BorderStyle="Solid" 
         ForeColor="Black" Height="50px">--%>
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataSourceID="SqlDataSource1"
                                    BorderColor="#999999" ForeColor="Black" Height="50px" DataKeyNames="RepairID"
                                    OnRowDataBound="OnRowDataBound" EmptyDataText="No records has been added.">
                                    <Columns>
                                        <asp:BoundField DataField="Repair_Desc" HeaderText="รายการแจ้งซ่อม (DESCRIPTION)"
                                            ItemStyle-Width="400px" />
                                        <asp:BoundField DataField="Repair_Cause" HeaderText="สาเหตุของอาการ (CAUSE OF PROBLEM)"
                                            ItemStyle-Width="400px" />
                                        <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"
                                            ItemStyle-Width="100px" />
                                    </Columns>
                                </asp:GridView>
                                <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                                    <tr>
                                        <td style="width: 400px">
                                            <%-- Repair_Desc:<br />--%>
                                            <asp:TextBox ID="txtRepair_Desc" runat="server" Width="400px" />
                                        </td>
                                        <td style="width: 400px">
                                            <%-- Repair_Cause:<br />--%>
                                            <asp:TextBox ID="txtRepair_Cause" runat="server" Width="400px" />
                                        </td>
                                        <td style="width: 100px">
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" class="btn btn-primary" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>"
                                    SelectCommand="SELECT RepairID ,ID,Repair_Desc ,Repair_Cause FROM TB_Repair where ID=@ID "
                                    InsertCommand="INSERT INTO TB_Repair (ID,Repair_Desc ,Repair_Cause, CreateDate) VALUES (@ID , @Repair_Desc, @Repair_Cause , getdate())"
                                    UpdateCommand="UPDATE TB_Repair SET Repair_Desc = @Repair_Desc, Repair_Cause = @Repair_Cause WHERE RepairID = @RepairID"
                                    DeleteCommand="DELETE FROM TB_Repair WHERE RepairID = @RepairID">
                                    <InsertParameters>
                                        <asp:ControlParameter Name="Repair_Desc" ControlID="txtRepair_Desc" Type="String" />
                                        <asp:ControlParameter Name="Repair_Cause" ControlID="txtRepair_Cause" Type="String" />
                                        <asp:ControlParameter Name="ID" ControlID="txtID" />
                                        <%--  <asp:controlparameter name="UserName" controlid=" lblUserName" />--%>
                                    </InsertParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="RepairID" Type="Int32" />
                                        <asp:Parameter Name="Repair_Desc" Type="String" />
                                        <asp:Parameter Name="Repair_Cause" Type="String" />
                                        <asp:ControlParameter Name="ID" ControlID="txtID" />
                                    </UpdateParameters>
                                    <DeleteParameters>
                                        <asp:Parameter Name="RepairID" Type="Int32" />
                                    </DeleteParameters>
                                    <SelectParameters>
                                        <asp:ControlParameter Name="ID" ControlID="txtID" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                            <div id="sectionC" class="tab-pane fade">
                                <%--  <asp:controlparameter name="UserName" controlid=" lblUserName" />--%>
                                <table>
                                    <tr>
                                        <td align="left" class="style13">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style13">
                                            <%--  <asp:controlparameter name="UserName" controlid=" lblUserName" />--%>
                                        </td>
                                        <td align="left" class="style1">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style12">
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style12">
                                            <asp:Label ID="Label3" runat="server" CssClass="fn_ContentTital" Text="Part No.  : "
                                                ToolTip="รหัสค่าแรง"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style5">
                                            <asp:DropDownList ID="drpPartNo" runat="server" Width="300px" class="form-control"
                                                ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="middle" class="style3">
                                            <asp:Label ID="lblDescriptionPart" runat="server" CssClass="fn_ContentTital" Text="Description"
                                                ToolTip="รายละเอียด" Width="100%"></asp:Label>
                                        </td>
                                        <td align="right" class="style6">
                                            <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" Text="Qty : " Width="30px"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style1">
                                            <asp:TextBox ID="txtQty" runat="server" Width="50px" Enabled="False"></asp:TextBox>
                                        </td>
                                        <td align="right" class="style1">
                                            <asp:Label ID="Label69" runat="server" CssClass="fn_ContentTital" Text="Price : "
                                                ToolTip="ราคา" Width="50px"></asp:Label>
                                        </td>
                                        <td align="left" class="style1">
                                            <asp:TextBox ID="txtUnitPrice" runat="server" Width="50px" Enabled="False"></asp:TextBox>
                                        </td>
                                        <td align="right" class="style1">
                                            <asp:Label ID="Label6" runat="server" CssClass="fn_ContentTital" Text="QTY Use: "
                                                ToolTip="ค่าแรง" Width="103px"></asp:Label>
                                        </td>
                                        <td align="left" class="style1">
                                            <asp:TextBox ID="txtQtyUse" runat="server" Width="50px" AutoPostBack="True">1</asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label87" runat="server" CssClass="fn_ContentTital" Text="Discount : "
                                                ToolTip="ค่าแรง" Width="66px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDiscount0" runat="server" Width="50px" TextMode="Number">0</asp:TextBox>
                                        </td>
                                        <td class="style2">
                                            <asp:DropDownList ID="drpDiscount0" runat="server" Height="30px" Width="70px">
                                                <asp:ListItem>Bath</asp:ListItem>
                                                <asp:ListItem Value="%">%</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" class="style1">
                                            &nbsp;
                                            <asp:Button ID="btnAddPart" runat="server" Text="ADD" class="btn btn-primary" autopostback="true" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="GridView3" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    DataKeyNames="PartID" DataSourceID="SqlDataSource3" Font-Bold="False" Font-Names="Tahoma"
                                    Font-Size="Small" ShowFooter="True" Width="100%" CellPadding="3" BackColor="White"
                                    BorderColor="#999999" GridLines="Vertical" BorderWidth="1px" BorderStyle="Solid"
                                    ForeColor="Black" Height="50px">
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="Black" Font-Bold="true" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                    <Columns>
                                        <asp:BoundField DataField="PartID" HeaderText="ID" SortExpression="PartID" ReadOnly="True">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartNo" HeaderText="Part No." SortExpression="PartNo">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="QtyUse" HeaderText="Qty" SortExpression="QtyUse">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" DataFormatString="{0:###,###.##}">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" DeleteImageUrl="~/Images/icon_delete.ico"
                                            EditImageUrl="~/Images/icon_edit.ico" ButtonType="Image" CancelImageUrl="~/Images/icon_close.ico"
                                            UpdateImageUrl="~/Images/icon_Save.ico">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:CommandField>
                                    </Columns>
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#808080" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#383838" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>"
                                    DeleteCommand="DELETE FROM  TB_PartRequired where PartID = @PartID" SelectCommand="SELECT PartID, PartNo	,Description  ,QtyUse , Price ,convert(varchar, Discount)+' ' + DiscountType as Discount ,(case when DiscountType = 'Bath' then (QtyUse*Price) - Discount when DiscountType = '%' then (QtyUse*Price) - (Discount/100)*(QtyUse*Price) end) as Amount   from TB_PartRequired where ID = @ID "
                                    UpdateCommand="UPDATE [TB_PartRequired] set  PartNo=@PartNo , Description=@Description, QtyUse=@QtyUse , Price=@Price , ModifyDate = getdate()  where [PartID] = @PartID ">
                                    <SelectParameters>
                                        <asp:ControlParameter Name="ID" ControlID="txtID" />
                                    </SelectParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="PartID" />
                                        <asp:Parameter Name="PartNo" />
                                        <asp:Parameter Name="Description" />
                                        <asp:Parameter Name="QtyUse" />
                                        <asp:Parameter Name="Price" />
                                    </UpdateParameters>
                                    <DeleteParameters>
                                        <asp:Parameter Name="PartID" />
                                    </DeleteParameters>
                                </asp:SqlDataSource>
                            </div>
                            <div id="sectionD" class="tab-pane fade">
                                <table>
                                    <tr>
                                        <td align="left" class="style13">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style13">
                                            <%--  <asp:controlparameter name="UserName" controlid=" lblUserName" />--%>
                                        </td>
                                        <td align="left" class="style1">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style12">
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style12">
                                            <asp:Label ID="Label1" runat="server" CssClass="fn_ContentTital" Text="Labor : "
                                                ToolTip="รหัสค่าแรง"></asp:Label>
                                        </td>
                                        <td align="left" valign="middle" class="style5">
                                            <asp:TextBox ID="txtLaborCode" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                        <td align="right" valign="middle" class="style3">
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style6">
                                            &nbsp;
                                        </td>
                                        <td align="right" class="style1">
                                            <asp:Label ID="Label67" runat="server" CssClass="fn_ContentTital" Text="Labor Rate : "
                                                ToolTip="ค่าแรง" Width="82px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLaborRate" runat="server" Width="70px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label68" runat="server" CssClass="fn_ContentTital" Text="Working Hours : "
                                                ToolTip="ค่าแรง" Width="103px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtHours" runat="server" Width="50px"></asp:TextBox>
                                        </td>
                                        <td align="right" class="style1">
                                            <asp:Label ID="Label86" runat="server" CssClass="fn_ContentTital" Text="Discount : "
                                                ToolTip="ค่าแรง" Width="66px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDiscount" runat="server" Width="50px" TextMode="Number">0</asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td align="left" class="style1">
                                            <asp:DropDownList ID="drpDiscount" runat="server" Height="30px" Width="70px">
                                                <asp:ListItem>Bath</asp:ListItem>
                                                <asp:ListItem Value="%">%</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="style1">
                                            <asp:Button ID="btnAddLabor" runat="server" Text="ADD" class="btn btn-primary" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    DataKeyNames="LaborID" DataSourceID="SqlDataSource2" Font-Bold="False" Font-Names="Tahoma"
                                    Font-Size="Small" ShowFooter="True" Width="100%" CellPadding="3" BackColor="White"
                                    BorderColor="#999999" GridLines="Vertical" BorderWidth="1px" BorderStyle="Solid"
                                    ForeColor="Black" Height="50px">
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="Black" Font-Bold="true" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                    <Columns>
                                        <asp:BoundField DataField="LaborID" HeaderText="No." SortExpression="LaborID" ReadOnly="True">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LaborCode" HeaderText="Labor Code" SortExpression="LaborCode">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DescriptionL" HeaderText="Description" SortExpression="DescriptionL">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LaborRate" HeaderText="Labor Rate" SortExpression="LaborRate">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="WorkingHours" HeaderText="Working Hours" SortExpression="WorkingHours">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" DataFormatString="{0:###,###.##}">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" DeleteImageUrl="~/Images/icon_delete.ico"
                                            EditImageUrl="~/Images/icon_edit.ico" ButtonType="Image" CancelImageUrl="~/Images/icon_close.ico"
                                            UpdateImageUrl="~/Images/icon_Save.ico">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:CommandField>
                                    </Columns>
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#808080" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#383838" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>"
                                    DeleteCommand="DELETE FROM  TB_Labor where LaborID = @LaborID" SelectCommand="SELECT LaborID	,LaborCode  ,DescriptionL , LaborRate , WorkingHours ,convert(varchar, Discount)+' ' + DiscountType as Discount ,(case when DiscountType = 'Bath' then (LaborRate*WorkingHours) - Discount when DiscountType = '%' then (LaborRate*WorkingHours) - (Discount/100)*(LaborRate*WorkingHours) end) as Amount from TB_Labor where ID = @ID "
                                    UpdateCommand="UPDATE [TB_Labor] set  LaborCode=@LaborCode, DescriptionL=@DescriptionL , LaborRate=@LaborRate , WorkingHours=@WorkingHours , ModifyDate = getdate()  where [LaborID] = @LaborID ">
                                    <SelectParameters>
                                        <asp:ControlParameter Name="ID" ControlID="txtID" />
                                    </SelectParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="LaborCode" />
                                        <asp:Parameter Name="DescriptionL" />
                                        <asp:Parameter Name="LaborRate" />
                                        <asp:Parameter Name="WorkingHours" />
                                    </UpdateParameters>
                                    <DeleteParameters>
                                        <asp:Parameter Name="LaborID" />
                                    </DeleteParameters>
                                </asp:SqlDataSource>
                            </div>
                            <%--  Ben --%>
                            <div id="sectionE" class="tab-pane fade">
                                <div class="row" style="margin-top:15px;"> 
                        
                                    <div class="col-md-6 text-right d-flex justify-content-end">
                                     <a href="#" class="btn btn-primary btn-sm" id="btadd_deposit"><i class="fa fa-plus"></i>เพิ่มเงินมัดจำ</a>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" scope="col">
                                                        No.
                                                    </th>
                                                    <th class="text-center"  scope="col">
                                                        จำนวน
                                                    </th>
                                                    <th class="text-center"  scope="col">
                                                        ว้นที่
                                                    </th>
                                                    <th class="text-center"  scope="col">
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="text-center"  scope="row">
                                                        1
                                                    </th>
                                                    <td  class="text-center" >
                                                        5,000
                                                    </td>
                                                    <td  class="text-center" >
                                                        2021-07-29
                                                    </td>
                                                    <td  class="text-center" >
                                                        <a class="btn btn-danger btn-sm btedit1"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                            <%--  Ben --%>
                        </div>
                        <hr />
                        <%--  <asp:controlparameter name="UserName" controlid=" lblUserName" />--%>
                        <br />
                        <div class="showback">
                            <asp:Button ID="btnCreateIncident" runat="server" Text="Create Repair Order" class="btn btn-primary" />
                            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-info" />
                            &nbsp;
                            <asp:Button ID="btnDelete" runat="server" Text="Cancel" class="btn btn-danger" Visible="False" />
                            &nbsp;
                            <%--<br />--%>&nbsp;<asp:Button ID="btnReport" runat="server" Text="Report" OnClientClick="SetTarget();"
                                class="btn btn-default" Visible="False" />
                            <%--<h4></h4>--%>&nbsp;<asp:Button ID="btnDO" runat="server" Text="Delivery Order Report"
                                OnClientClick="SetTarget();" class="btn btn-default" Visible="False" />
                        </div>
                        <br />
                        <!--BEGIN FOOTER-->
                        <div id="footer">
                            <div class="copyright">
                                <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                        </div>
                        <!--END FOOTER-->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
                <!-- /#wrapper -->
                <!-- jQuery -->
                <%--<h4></h4>--%>
                <!-- Bootstrap Core JavaScript -->
                <%--End TAB--%>
                <!-- Morris Charts JavaScript -->
                <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                            class="btn btn-warning" Visible="False" />--%>
                <%--<button type="button" class="btn btn-warning">Warning</button>--%>
                <link href="dropdown/select2.min.css" rel="stylesheet" />
                <script src="dropdown/select2.min.js" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpCust").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpItem").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpSerialNumber").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpClassisNumber").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpReceptionnist").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpStatus").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpTechnician").select2({
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#drpPartNo").select2({
                        });
                    });
                </script>
                <script src="date/jquery-1.7.min.js"></script>
                <script src="date/jquery-ui.min.js"></script>
                <link type="text/css" rel="Stylesheet" href="date/jquery-ui.css" />
                <script type="text/javascript">
                    //ประกาศตัวแปรเพื่อรับใหม่
                    $jq = $.noConflict();
                    $jq().ready(

                 function () {
                     $jq('#<%=txtDateRepair.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'date/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });     
                </script>
    </form>

    
<!-- Modal -->
<div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">เพิ่ม</h2>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-8">
                        <label for="" class="form-label">ใส่จำนวนเงินมัดจำ</label>
                        <input class="form-control" id="modal_txtjobcode" value="" style="width:100%;">
                    </div>
                    <div class="col-md-4 text-left">
                        <label for="" class="form-label">.</label>
                       <h4 class="modal-title">บาท</h4>
                    </div>
            

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modalclose" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal_btsave">บันทึก</button>
            </div>
        </div>
    </div>
</div>


</body>
</html>
