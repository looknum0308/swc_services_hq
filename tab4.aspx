﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tab4.aspx.vb" Inherits="SWC_ServiceSystem.tab4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script> 

<script>
    $(document).ready(function () {
        
        $.ajax({ url: "ajax/jquery.txt", success: function (tabdata) {
            $("#jquerytab").append(tabdata);
        } 
        });

    });
</script>
<style>
.nav-tabs
 {
   border-color:#004A00;
   width:60%;
 }

.nav-tabs > li a { 
    border: 1px solid #004A00;
    background-color:#004A00; 
    color:#fff;
    }

.nav-tabs > li.active > a,
.nav-tabs > li.active > a:focus,
.nav-tabs > li.active > a:hover{
    background-color:#D5FFD5;
    color:#000;
    border: 1px solid #1A3E5E;
    border-bottom-color: transparent;
    }

.nav-tabs > li > a:hover{
  background-color: #D5FFD5 !important;
    border-radius: 5px;
    color:#000;

} 

.tab-pane {
    border:solid 1px #004A00;
    border-top: 0; 
    width:60%;
    background-color:#D5FFD5;
    padding:5px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
  
  <h3>A demo to load data from text file in Bootstrap Tab by $.ajax</h3>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li><a href="#hometab" role="tab" data-toggle="tab">Home</a></li>
  <li><a href="#bstrap" role="tab" data-toggle="tab">Bootstrap</a></li>
  <li><a href="#csstab" role="tab" data-toggle="tab">CSS</a></li>
 <%-- <li class="active"><a href="#jquerytab" role="tab" data-toggle="tab">jQuery</a></li>--%>
   <li><a href="#jquerytab" role="tab" data-toggle="tab">jQuery</a></li>

</ul>


<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane" id="hometab">Write something for home tab</div>
  <div class="tab-pane" id="bstrap">Bootstrap is a mobile first web framework</div>
  <div class="tab-pane" id="csstab">

 <%-- The CSS is used to style web pages!--%>
     <table >
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13">

                <%--<h4></h4>--%>
                    </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            <tr>
                <td align="right" class="style12">
                    &nbsp;</td>
                <td align="right" class="style12">
                    <asp:Label ID="Label1" runat="server" CssClass="fn_ContentTital" 
                        Text="Labor Code : " ToolTip="รหัสค่าแรง"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style5">


                <asp:DropDownList ID="txtLaborCode" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>


                    </td>
                <td align="right" valign="middle" class="style3">

                    <asp:Label ID="Label2" runat="server" CssClass="fn_ContentTital" 
                        Text="Description : " ToolTip="รายละเอียด" Width="84px"></asp:Label>
                    </td>
                <td align="left" class="style6" colspan="3">

                    <asp:TextBox ID="txtDescription" runat="server" Width="100%" Enabled="False"></asp:TextBox>
                    </td>
                <td align="right" class="style1">

                    <asp:Label ID="Label67" runat="server" CssClass="fn_ContentTital" 
                        Text="Labor Rate : " ToolTip="ค่าแรง" Width="82px"></asp:Label>

                    </td>
                <td>


                    <asp:TextBox ID="txtLaborRate" runat="server" Width="100%" Enabled="False"></asp:TextBox>


                </td>
                <td>

                    <asp:Label ID="Label68" runat="server" CssClass="fn_ContentTital" 
                        Text="Working Hours : " ToolTip="ค่าแรง" Width="103px"></asp:Label>

                    </td>
                <td>


                    <asp:TextBox ID="txtHours" runat="server" Width="100%"></asp:TextBox>


                    </td>
                <td align="left" class="style1">

                &nbsp;

                <asp:Button ID="btnAddLabor" runat="server" Text="ADD" class="btn btn-primary" />

                    </td>
            </tr>       
          
        </table>

  </div>
  <div class="tab-pane active" id="jquerytab"></div>
  
</div>

    </form>
</body>
</html>


