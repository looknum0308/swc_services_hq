﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Web.CrystalReportViewerBase
Imports System.Data.OleDb
Imports System.IO
Imports CrystalDecisions.ReportSource
Imports System.Web.UI.WebControls.Table

Public Class RepairOrderReport
    Inherits System.Web.UI.Page
    Dim db As New Connect_HR
    Dim ids As String
    Dim uid As String
    Dim uapp As String
    Dim usr() As String
    Dim sql As String
    Dim dt As New DataTable
    Dim sess As String
    Dim sqlPara1 As SqlParameter
    Dim sqlPara2 As SqlParameter
    Dim fname As String
    Dim Formula As FormulaFieldDefinition
    Dim sqlReport, Form As String
    Private sms As New PKMsg("")
    Dim db_SWC As New Connect_Service
    Dim cryRpt As New ReportDocument

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As Table


        Form = Request.QueryString(0)
        ID = Request.QueryString(1)



        If Form = "RepariOrder" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_RepairOrder.rpt"))

            sql = " select * "
            sql += " from [TB_Repair] "
            sql += " where [ID] = '" & ID & "' "
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                cryRpt.Load(Server.MapPath("RPT_RepairOrder.rpt"))
            Else
                cryRpt.Load(Server.MapPath(" RPT_RepairOrderNULL.rpt"))
            End If

        ElseIf Form = "PM50" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_WorkOrderPM50.rpt"))
            cryRpt.Load(Server.MapPath("RPT_WorkOrderPM50.rpt"))
        ElseIf Form = "PM100" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_WorkOrderPM100.rpt"))
            cryRpt.Load(Server.MapPath("RPT_WorkOrderPM100.rpt"))
        ElseIf Form = "DO" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_DeliveryOrder.rpt"))
            cryRpt.Load(Server.MapPath("RPT_DeliveryOrder.rpt"))
        End If

  
        Call CreateCondition()
        If sql <> "" Then
            cryRpt.RecordSelectionFormula = sql
        End If


        With crConnectionInfo
            .ServerName = "10.0.4.20"
            .DatabaseName = "DB_SwcService"
            .UserID = "armth"
            .Password = "Kb5r#Ge9Z3M*mQ"
        End With


        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next

        If Form = "RepariOrder" Then
            CrystalReportViewer1.ID = "RepairOrder_" & ID & "_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "PM50" Then
            CrystalReportViewer1.ID = "RepairOrder_" & ID & "_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "PM100" Then
            CrystalReportViewer1.ID = "RepairOrder_" & ID & "_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "DO" Then
            CrystalReportViewer1.ID = "DeliveryOrder_" & ID & "_" & Date.Now.ToString("dd/MM/yyyy")

        End If

        CrystalReportViewer1.ReportSource = cryRpt
        CrystalReportViewer1.RefreshReport()


        ''add 8/1/2564
        'cryRpt.Close()
        'cryRpt.Dispose()
        'GC.Collect()

    End Sub

    Protected Overrides Sub OnUnload(e As EventArgs)
        MyBase.OnUnload(e)

        cryRpt.Close()
        cryRpt.Dispose()
        GC.Collect()
    End Sub

    Sub CreateCondition()
        If id <> "" Then


            If Form = "RepariOrder" Then
                sql = " {VW_RepairOrder.ID} = '" & ID & "' "

            ElseIf Form = "PM50" Then
                sql = " {VW_PM50.ID} = '" & ID & "' "

            ElseIf Form = "PM100" Then
                sql = " {VW_PM100.ID} = '" & ID & "' "

            ElseIf Form = "DO" Then
                sql = " {VW_DeliveryOrder.ID} = '" & ID & "' "

            End If


        End If
    End Sub

End Class