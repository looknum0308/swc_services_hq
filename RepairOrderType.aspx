﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RepairOrderType.aspx.vb" Inherits="SWC_ServiceSystem.RepairOrderType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Service System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


     <%--TAB--%>
<%--     http://www.tutorialrepublic.com/faq/how-to-keep-the-current-tab-active-on-page-reload-in-bootstrap.php--%>
<%--<link rel="stylesheet" href="css/bootstrap.min.css">--%>
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>
</head>
<body>
    <form id="form1" runat="server">
     <div id="wrapper">

       
            <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav side-nav">
                   
                   <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                    <%--  <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>  --%>  

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Repair Order <small></small>
                        </h2>
                        
                     <section class="panel">
                          <header class="panel-heading">
                             เลือกประเภทงาน Service
                          </header>
                          <div class="panel-body">
                              <form class="form-horizontal " method="get">
                                  <div class="form-group">
                                      <label class="col-md-2 control-label"></label>
											<div class="col-md-10">
											
											    <asp:LinkButton ID="LinkButton1" runat="server"><h4>1. Repair Order (ซ่อมทั่วไป)</h4></asp:LinkButton>

                                                </br>
												
                                                <asp:LinkButton ID="LinkButton2" runat="server"><h4>2. Preventive Maintenance (PM)</h4></asp:LinkButton>

                                                 </br>
												
                                                <asp:LinkButton ID="LinkButton3" runat="server"><h4>3. Claim (ไม่คิดค่าใช้จ่าย)</h4></asp:LinkButton>

                                                 </br>
												
                                                <asp:LinkButton ID="LinkButton4" runat="server"><h4>4. Campaign  (ไม่คิดค่าใช้จ่าย)</h4></asp:LinkButton>
                                                
											</div>
                                  </div>                                 
                              </form>
                          </div>					
                      </section>          

                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">

    

     


           

                <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->



    <!-- jQuery -->
<%--    <script src="js/jquery.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
<%--    <script src="js/bootstrap.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
 <%--   <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>--%>


      

    <%--        dropdown--%><%--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>--%>
                    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                    
                   
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpCust").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpItem").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpSerialNumber").select2({
                             });
                         });
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpClassisNumber").select2({
                            });
                        });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpReceptionnist").select2({
                             });
                         });
                    </script>
                    
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpStatus").select2({
                            });
                        });
                    </script>
    </form>
</body>
</html>
